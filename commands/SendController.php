<?php

namespace app\commands;

use Yii;
use yii\console\Controller;
use yii\console\ExitCode;
use app\models\Messages;
/*
*
*/
use app\models\Autos;
use app\models\Reliz;
use app\models\Containers;
use app\models\Calc;

use app\models\Logger;

class SendController extends Controller
{
    

    private function rus2eng($text) 
    {
        $russian = [];
        $replace = [
            'А' => 'A',
            'В' => 'B',
            'Е' => 'E',
            'Ё' => 'E',
            'К' => 'K',
            'М' => 'M',
            'Н' => 'H',
            'О' => 'O',
            'Р' => 'P',
            'С' => 'C',
            'Т' => 'T',
            'У' => 'Y',
            'Х' => 'X',
        ];
        foreach($replace as $k => $v) 
        {
            $russian[] = '/'.$k.'/ui';
        }
        $result = preg_replace($russian, $replace, $text);
        return $result;    
    }

    public function actionMessages()
    {

        $messages = Messages::findAll(['send' => null]);
        
        foreach ($messages as $message)
        {
            $mail = Yii::$app->mailer->compose();

            $mail->setTo(explode(';', $message->to));
            $mail->setFrom($message->from);

            $mail->setSubject($message->subject);
            $mail->setTextBody($message->body);

            $mail->attachContent($message->body, ['fileName' => 'attach.txt', 'contentType' => 'text/plain']);

            $mail->send();

            $message->send = date("Y-m-d H:i:s");
            $message->save();
        }

        //echo var_dump($messages);
        

        return ExitCode::OK;
    }

    public function actionXml()
    {

        libxml_use_internal_errors(true);

        $config = Yii::$app->params['mail'];

        $inbox = imap_open($config['hostname'], $config['username'], $config['password']) 
            or die('Cannot connect to email: ' . imap_last_error());

        $emails = imap_search($inbox, 'UNSEEN');

        if($emails) 
        {
            foreach($emails as $email_number)
            {
                $overview = imap_fetch_overview($inbox, $email_number, 0);
                $message = imap_fetchbody($inbox, $email_number, 2);
                $structure = imap_fetchstructure($inbox, $email_number);

                $attachments = array();

                if(isset($structure->parts) && count($structure->parts))
                {
                    for($i = 0; $i < count($structure->parts); $i++)
                    {
                        $attachments[$i] = array(
                            'is_attachment' => false,
                            'filename'      => '',
                            'name'          => '',
                            'attachment'    => ''
                        );

                        if($structure->parts[$i]->ifdparameters)
                        {
                            foreach($structure->parts[$i]->dparameters as $object)
                            {
                                if(strtolower($object->attribute) == 'filename')
                                {
                                    $attachments[$i]['is_attachment'] = true;
                                    $attachments[$i]['filename'] = $object->value;
                                }
                            }
                        }

                        if($structure->parts[$i]->ifparameters)
                        {
                            foreach($structure->parts[$i]->parameters as $object)
                            {
                                if(strtolower($object->attribute) == 'name')
                                {
                                    $attachments[$i]['is_attachment'] = true;
                                    $attachments[$i]['name'] = $object->value;
                                }
                            }
                        }

                        if($attachments[$i]['is_attachment'])
                        {
                            $attachments[$i]['attachment'] = imap_fetchbody($inbox, $email_number, $i+1);

                            /* 4 = QUOTED-PRINTABLE encoding */
                            if($structure->parts[$i]->encoding == 3)
                            {
                                $attachments[$i]['attachment'] = base64_decode($attachments[$i]['attachment']);
                            }
                            /* 3 = BASE64 encoding */
                            elseif($structure->parts[$i]->encoding == 4)
                            {
                                $attachments[$i]['attachment'] = quoted_printable_decode($attachments[$i]['attachment']);
                            }
                        }
                    }
                }
                foreach($attachments as $attachment)
                {
                    if($attachment['is_attachment'] == 1)
                    {
                        $this->insert($attachment);
                    }
                }
            }
        }

        imap_close($inbox);
        return ExitCode::OK;
    }

    private function insert($attachment)
    {

        $file_iterator = glob(dirname(__FILE__) .'/../files/*.*');
        $iterator_count = count($file_iterator);

        $file_name = 'files/' . ++$iterator_count . '-' . $attachment['filename'];
        file_put_contents($file_name, $attachment['attachment']);

        $xml = simplexml_load_file($file_name);
        if ($xml !== false)
        {
            if ($xml->DocHead->Modification == 'Original') 
            {
                if ($xml->Containers->NumberedPosLimit == 0) 
                {
                    $stock = addslashes(ucfirst(strtolower($xml->Agent->Name)));

                    $e = explode(' ', $xml->Containers->NotNumbered->MwdetspecCode);
                            
                    $xmlTypeContainer = strtolower($xml->Containers->NotNumbered->MwdetspecCode);
                    if ($xmlTypeContainer == 're 40') 
                    {
                        $type_cnt = '40RF';
                    } else if ($xmlTypeContainer == 're 20' )
                    {
                        $type_cnt = '20RF';
                    } else {
                        $type_cnt = addslashes($e[1] . $xml->Containers->NotNumbered->HeightCode);
                    }
                            
                    $comment = isset($xml->Comment) ? addslashes($xml->Comment) : '';
                            
                    $amount = addslashes($xml->Containers->NotNumbered->Count);
                    $rest = $amount;

                    $date = addslashes($xml->StartDate);
                    $date_to = addslashes($xml->ExpireDate);
                    $number = addslashes($xml->DocHead->ReleaseNumber);
                    $buking = addslashes($xml->DocHead->DocNumber);

                    $reliz = new Reliz;
                    $reliz->stock = $stock;
                    $reliz->type_cnt = $type_cnt;
                    $reliz->amount = $amount;
                    $reliz->rest = $rest;
                    $reliz->date = $date;
                    $reliz->date_to = $date_to;
                    $reliz->number = $number;
                    $reliz->buking = $buking;
                    $reliz->method = $xml->DischargeType == 'автотранспорт' ? 1 : 0;
                    $reliz->type = 0;
                    $reliz->status = 0;
                    $reliz->comment = $comment;
                    
                    // Отправка письма клиенту об успешном принятии релиза
                    if ($reliz->save()) {
                        $mail = Yii::$app->mailer->compose();
                        $mail->setTo(['mercworkorder@edi.maersk.com','tech@cntdepot.ru','matveev@cntdepot.ru','theakak1@gmail.com']);
                        $mail->setBcc('edi@depot.solutions');
                        $mail->setFrom('edi@depot.solutions');
                        $mail->setTextBody('Reliz');
                        $mail->setSubject('Reliz номер '.$reliz->number.' был принят в обработку');
                        $mail->send();
                    }
                    
                               
                    foreach ($xml->TruckNumbers->Truck as $truck) 
                    {
                        $number = addslashes($truck->Number);
                        $driver = addslashes($truck->Driver);
                                                                        
                        $auto = new Autos;
                        $auto->reliz_id = $reliz->id;
                        $auto->number = $this->rus2eng($number);
                        $auto->driver = $driver;

                        $auto->save();
                    }

                }
            }
            else if ($xml->DocHead->Modification == 'Cancellation') 
            {
                $number = $xml->DocHead->ReleaseNumber;

                $reliz = Reliz::find()->where(['number' => $number])->one();
                if ($reliz == null)
                    return;
                $reliz->status = 3;
                $log_text = 'Релиз с ID#' . $reliz->id . ' был успешно удален' . PHP_EOL;

                $autos = Autos::find()->where(['reliz_id' => $reliz->id])->all();
                if ($autos !== null)
                {
                    $log_text .= PHP_EOL . 'Связные машины:' . PHP_EOL;
                    foreach ($autos as $auto) {
                            $text .= $auto->number;
                            $text .= !empty($auto->driver) ?  ' - ' . $auto->driver : '';
                            $text .= PHP_EOL;
                    }
                    $autos->deleteAll();
                }

                $containers = Containers::find()->where(['reliz' => $reliz->number])->all();
                if ($containers !== null)
                {
                    $text .= PHP_EOL . 'Связные контейнеры:';
                    foreach ($containers as $container) {
                        $text .= $container->number . PHP_EOL;
                    }
                }

                $reliz->setLogText('remove','Релиз с ID#' . $reliz->id . ' был успешно удален');

                // Отправка письма клиенту об успешном удалении релиза
                if ($reliz->save()) {
                    $mail = Yii::$app->mailer->compose();
                    $mail->setTo(['mercworkorder@edi.maersk.com','tech@cntdepot.ru','matveev@cntdepot.ru','theakak1@gmail.com']);
                    $mail->setBcc('edi@depot.solutions');
                    $mail->setFrom('edi@depot.solutions');
                    $mail->setTextBody('Reliz');
                    $mail->setSubject('Reliz номер '.$reliz->number.' был принят в обработку');
                    $mail->send();
                }
            }
            else if ($xml->DocHead->Modification == 'Replace') 
            {
                $stock = ucfirst(strtolower($xml->Agent->Name));
                $e = explode(' ', $xml->Containers->NotNumbered->MwdetspecCode);
                $type_cnt = $e[1] . $xml->Containers->NotNumbered->HeightCode;
                $amount = $xml->Containers->NotNumbered->Count;
                $rest = $amount;
                $number = $xml->DocHead->ReleaseNumber;
                $buking = $xml->DocHead->DocNumber;
                $method = $xml->DischargeType == 'автотранспорт' ? 1 : 0;
                    
                
                $xmlTypeContainer = strtolower($xml->Containers->NotNumbered->MwdetspecCode);
                if ($xmlTypeContainer == 're 40') 
                {
                    $type_cnt = '40RF';
                } else if ($xmlTypeContainer == 're 20' )
                {
                    $type_cnt = '20RF';
                } else {
                    $type_cnt = $e[1] . $xml->Containers->NotNumbered->HeightCode;
                }
                
                $status = $type = 0;

                $reliz = Reliz::find()->where(['number' => $number])->one();
                $containers = Containers::find()->where(['reliz' => $number])->all();

                foreach ($containers as $container) {
                    $container->am_number = rus2eng($container->am_number);
                    $container->am2 = rus2eng($container->am2);
                }
                        
                $reliz->date = strtotime($xml->StartDate) == strtotime($reliz->date) ? : $xml->StartDate; 
                $reliz->date_to = strtotime($date_to) == strtotime($xml->ExpireDate) ? : $xml->ExpireDate; 
                $reliz->type = strtotime($type_cnt) == strtotime($reliz->type_cnt) ? : $type_cnt; 
                // Отправка письма клиенту об успешном принятии релиза
                if ($reliz->save()) {
                    $mail = Yii::$app->mailer->compose();
                    $mail->setTo(['mercworkorder@edi.maersk.com','tech@cntdepot.ru','matveev@cntdepot.ru','theakak1@gmail.com']);
                    $mail->setBcc('edi@depot.solutions');
                    $mail->setFrom('edi@depot.solutions');
                    $mail->setTextBody('Reliz'.$reliz->number);
                    $mail->setSubject('Reliz номер '.$reliz->number.' был принят в обработку');
                    $mail->send();
                }            

                $truckNumbers = [];
                $log_text = '';

                $autos = Autos::find()->where(['reliz_id' => $reliz->id])->all();

                if ($autos) 
                {
                    $log_text .= 'Удалены машины:' . PHP_EOL;
                    foreach($autos as $auto) 
                    {
                        $log_text .= $auto->number . PHP_EOL;
                    }
                    $autos->deleteAll();
                }

                if ($xml->TruckNumbers->count() > 0) 
                {
                    $log_text .= 'Добавлены машины:' . PHP_EOL;
                    foreach($xml->TruckNumbers->Truck as $truck) 
                    {
                        $auto = (string) $truck->Number;
                        if (!empty($auto)) 
                        {
                            $a = new Autos;
                            $a->reliz_id = $reliz->id;
                            $a->number = rus2eng($auto);
                            $a->save();
                            $log_text .= rus2eng($auto) . PHP_EOL;
                        }
                    }
                }
                
                if (!empty($text)) 
                {
                    Logger::addLog(
                        'reliz', 
                        'edit', 
                        'Изменения в релизе ' . $reliz->number, 
                        $log_text
                    );
                }
            }
        }
    }

    public function actionTest()
    {
        libxml_use_internal_errors(true);

        $config = Yii::$app->params['mail'];

        $inbox = imap_open($config['hostname'], $config['username'], $config['password']) 
            or die('Cannot connect to email: ' . imap_last_error());

        $emails = imap_search($inbox, 'SUBJECT "ESTIMATE" UNSEEN');

        if ($emails) {
            foreach ($emails as $email_number) {
                $header = imap_headerinfo($inbox, $email_number);
                $message = imap_fetchbody($inbox, $email_number, 1);
                $subject = explode(' ', $header->subject);

                // Находим номер контейнера
                $number_reg = preg_match('/[A-Za-z]{4}[0-9]+/', $message, $number);

                // Находим id сметы
                preg_match('/\/[0-9\s]+/', $header->subject, $calc_id);
                $calc_id = trim(str_replace('/','',$calc_id[0]));


                // Находим код магазина
                //$shop_code_reg = preg_match('/SHOP CODE: [A-Z0-9]{3}/', $message, $shop_code);


                //$number[0] = 'MSKU2422510';
                //$number[0] = 'MRKU8327441';
                

                // находим контейнер по номеру
                $customer = Containers::find()
                ->where(['number' => $number[0]])
                ->orderBy('id DESC')
                ->one();

                if ($customer) {
                    // Находим смету по id контейнера
                    $contract = Calc::find()
                    ->where(['container_id' => $customer->id])
                    ->andWhere(['id' => $calc_id])
                    ->orderBy('id DESC')
                    ->one();

                    // Присваиваем полю error(надо создать) в таблице Calc цифровое значение ошибки(создать массив с ошибками их обозначениями)
                    if ($contract) {
                        $contract->error_code = 1;
                        if (in_array('ERROR', $subject)) {
                            $error_text = explode("\n\r", $message);
                            $contract->error_code = 2;
                            $contract->error_text = $error_text[0];
                        }
                        
                        $contract->save();
                    }
                    
                }
                // Если существует $cnt_number1 (номер контейнера) и номер магазина соответствует prime ($shop_res)
                // ТО извлекаем информацию о смете (calc) по его номеру и добавляем код ошибки в новое поле error
                // при выводе контейнеров на странице со всеми контейнерами извелкаем информацию о смете по id контейнера(done)
                // после этого применяем стили к записи контейнера в зависимости от кода ошибки rowOptions
            }
        }

        imap_close($inbox);
        return ExitCode::OK;
    }

    // Разрешения в RBAC
    public function actionPermissions() {
        $auth = Yii::$app->authManager;

        // Создание контейнера
        // $createCnt = $auth->createPermission('createContainer');
        // $createCnt->description = 'Создание контейнера';
        // $auth->add($createCnt);

        // Редактирование контейнера
        // $updateCnt = $auth->createPermission('updateContainer');
        // $updateCnt->description = 'Изменение контейнера';
        // $auth->add($updateCnt);

        // // Удаление контейнера
        // $deleteCnt = $auth->createPermission('deleteContainer');
        // $deleteCnt->description = 'Удаление контейнера';
        // $auth->add($deleteCnt);

        // // Экспорт контейнеров в эксель
        // $exportCnt = $auth->createPermission('exportContainer');
        // $exportCnt->description = 'Экспорт контейнера в ексель';
        // $auth->add($exportCnt);

        // // Отгрузка контейнеров
        // $sheepCnt = $auth->createPermission('sheepContainer');
        // $sheepCnt->description = 'Отгрузка контейнера';
        // $auth->add($sheepCnt);

        // // Блокировка контейнеров
        // $blockCnt = $auth->createPermission('blockContainer');
        // $blockCnt->description = 'Блокировка контейнера';
        // $auth->add($blockCnt);

        // // Просмотр сметы контейнеров
        // $contractCnt = $auth->createPermission('contractContainer');
        // $contractCnt->description = 'Просмотр сметы контейнера';
        // $auth->add($contractCnt);

        // // Сохранение сметы контейнеров
        // $saveContractCnt = $auth->createPermission('saveContractContainer');
        // $saveContractCnt->description = 'Сохранение сметы контейнера';
        // $auth->add($saveContractCnt);
        $admin = $auth->getRole('Администратор');
        $auth->assign($admin, 7);
    }
}
