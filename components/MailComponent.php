<?php
 
namespace app\components;

use Yii;
use yii\base\Component;
use app\models\Containers;
use app\models\Messages;
use app\models\Users;
 
class MailComponent extends Component 
{
 	
 	private static $from = 'edi@depot.solutions';
 	private static $subject = 'CODECO';


	public static function addContainer($container)
	{
		$stock = $container->stock_r;
 		$type = $container->type_r;
 		$user = $stock->user;

 		$date_in_first = str_replace("-", "", $container->date_in);
        $date_in_first = substr($date_in_first, 2);
        $date_in_first = $date_in_first . ":" . str_replace(":", "", $container->time_in);
		$date_in_first = substr($date_in_first, 0, 11);

 		$date_in = str_replace("-", "", $container->date_in);
		$date_in = $date_in . str_replace(":", "", $container->time_in);
		$date_in = substr($date_in, 0, 12);

		$iso = $type->iso;
		if ($user->edi)
		{
			$body  = 'UNB+UNOA:1+' . strtoupper($stock->segment) . '+' . strtoupper($stock->name) . '+' . $date_in_first . ':1021+' . $container->id . '\'' . PHP_EOL;
			$body .= 'UNH+' . $container->id . '+CODECO:D:95B:UN:\'' . PHP_EOL;
			$body .= 'BGM+34+' . $container->id . '+9\'' . PHP_EOL;
			$body .= 'NAD+CA+MSK\'';
			$body .= 'EQD+CN+' . $container->number . '+' . $iso . ':102:5+++4\'' . PHP_EOL;
			$body .= 'RFF+BN:' . $container->buking . '\'' . PHP_EOL;
			$body .= 'DTM+7:' . $date_in . ':203\'' . PHP_EOL;
			$body .= 'LOC+165+MKT:139:6+' . $stock->segment . ':TER:ZZZ\'' . PHP_EOL;
			$body .= 'TDT+1++3+31++++A001AA\'' . PHP_EOL;
			$body .= 'CNT+16:1\'' . PHP_EOL;
			$body .= 'UNT+13+' . $container->id . '\'' . PHP_EOL;
			$body .= 'UNZ+1+' . $container->id . '\'' . PHP_EOL;

			self::sendToDB($user, strtoupper($body));
		}
	}

 	public static function editContainer($container)
 	{
 		$stock = $container->stock_r;
 		$type = $container->type_r;
 		$user = $stock->user;

 		$date_in = str_replace("-", "", $container->date_in);
		$date_in = $date_in . str_replace(":", "", $container->time_in);
		$date_in = substr($date_in, 0, 12);

		$iso = $type->iso;
		if ($user->edi)
		{
			$body  = 'UNB+UNOA:1+' . strtoupper($stock->segment) . '+' . strtoupper($stock->name) . '+' . date("ymd+Hi") . ':1021+' . $container->id . '\'' . PHP_EOL;
			$body .= 'UNH+' . $container->id . '+CODECO:D:95B:UN:\'' . PHP_EOL;
			$body .= 'BGM+36+' . $container->id . '+9\'' . PHP_EOL;
			$body .= 'NAD+CA+MSK\'' . PHP_EOL;
			$body .= 'EQD+CN+' . $container->number . '+' . $iso . ':102:5+++4\'' . PHP_EOL;
			$body .= 'RFF+BN:' . $container->buking . '\'' . PHP_EOL;
			$body .= 'DTM+7:' . $date_in . ':203\'' . PHP_EOL;
			$body .= 'LOC+165+MKT:139:6+' . $stock->segment . ':TER:ZZZ\'' . PHP_EOL;
			$body .= 'TDT+1++3+31++++A001AA\'' . PHP_EOL;
			$body .= 'CNT+16:1\'' . PHP_EOL;
			$body .= 'UNT+13+' . $container->id . '\'' . PHP_EOL;
			$body .= 'UNZ+1+' . $container->id . '\'' . PHP_EOL;

			self::sendToDB($user, strtoupper($body));
			//self::sendToEmail('theakak1@gmail.com', strtoupper($body));
		}
	}

	public static function shipContainer($container)
	{
		$stock = $container->stock_r;
 		$type = $container->type_r;
 		$user = $stock->user;

 		$date_in = str_replace("-", "", $container->date_in);
		$date_in = $date_in . str_replace(":", "", $container->time_in);
		$date_in = substr($date_in, 0, 12);

		$format_date = date("ymd+Hi", $date_in);

		$iso = $type->iso;
		if ($user->edi)
		{
			$body  = 'UNB+UNOA:1+' . strtoupper($stock->segment) . '+' . strtoupper($stock->name) . '+' . $format_date . '+' . $container->id . '\'' . PHP_EOL;
			$body .= 'UNH+' . $container->id . '+CODECO:D:95B:UN:\'' . PHP_EOL;
			$body .= 'BGM+36+' . $container->id . '+9\'' . PHP_EOL;
			$body .= 'NAD+CA+MSK\'' . PHP_EOL;
			$body .= 'EQD+CN+' . $container->number . '+' . $iso . ':102:5+++4\'' . PHP_EOL;
			$body .= 'RFF+BN:' . $container->buking . '\'' . PHP_EOL;
			$body .= 'DTM+7:' . $date_in . ':203\'' . PHP_EOL;
			$body .= 'LOC+165+MKT:139:6+' . $stock->segment . ':TER:ZZZ\'' . PHP_EOL;
			$body .= 'TDT+1++3+31++++A001AA\'' . PHP_EOL;
			$body .= 'CNT+16:1\'' . PHP_EOL;
			$body .= 'UNT+13+' . $container->id . '\'' . PHP_EOL;
			$body .= 'UNZ+1+' . $container->id . '\'' . PHP_EOL;

			self::sendToDB($user, strtoupper($body));
		}
	}

	private static function sendToEmail($emails, $body)
	{
		//$to = implode(";", $emails);
		$to = $emails;

		Yii::$app->mailer->compose()
            ->setTo($to)
            ->setFrom([self::$from])
            ->setSubject(self::$subject)
            ->setTextBody($body)
            ->send();
	}

	private static function sendToDB($user, $body)
	{
		$to = $user->org_r->email;
		
		if ($to === null)
			return false;

		$message = Messages::find()
			->where([ 'to' => $to, 'send' => null])
			->one();

		if($message === null) 
		{
			$message = new Messages;
			$message->body = $body;
		} else {
			$message->body .= $body;
		}
		$message->subject 	= self::$subject;
		$message->from 		= self::$from;
		$message->to 		= $to;
		$message->create 	= Yii::$app->formatter->asDate('now', 'yyyy-MM-dd HH:mm:ss');
		$message->save();
		return true;
	}

	private static function sendToFile($emails, $body)
	{
		$to = implode(";", $emails);
		$file_name = base64_encode($to);

		$path = dirname(__FILE__) . '/../attachments/codes/';

		$file = fopen($path . $file_name, 'a');  
		fwrite($file, $body); 
		fclose($file);
	}

}