<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use common\models\LoginForm;

class BaseController extends Controller
{

    public function beforeAction($action)
    {
        if (Yii::$app->user->isGuest) 
        {
            $this->redirect(['site/login']);
        }
        if (!parent::beforeAction($action)) 
        {
            return false;
        }
        return true;
    }

    function compare($new_data, $old_data) 
    {
        $result = [];

        $attributes = $old_data->attributeLabels();
        $old_data   = $old_data->getAttributes();

        foreach ($new_data->getAttributes() as $k => $n) 
        {
            if (!isset($old_data[$k])) 
                continue;
            
            if ($n != $old_data[$k]) 
            {
                $result[$attributes[$k]] = $old_data[$k].' -> ' . $n;
            }           
        }
        return $result;
    }

    function compareToLog($new_data, $old_data) 
    {
        $result = '';

        $attributes = $old_data->attributeLabels();
        $old_data   = $old_data->getAttributes();

        foreach ($new_data->getAttributes() as $k => $n) 
        {
            if (!isset($old_data[$k])) 
                continue;
            
            if ($n != $old_data[$k]) 
            {
                $result .= $attributes[$k] . ': ' . $old_data[$k].' -> ' . $n . PHP_EOL;
            }           
        }
        return $result;
    }

    function createToLog($model)
    {
        $result = '';

        $attributes = $model->attributeLabels();

        foreach ($model->getAttributes() as $k => $n) 
        {
            if ($k === 'id' || !isset($attributes[$k]))
                continue;
            $result .= $attributes[$k] . ': '. $n . PHP_EOL;       
        }
        return $result;
    }
    
}