<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\Containers;
use app\models\CheckSearch;
use app\models\Users;
use app\models\Autos;
use app\models\Reliz;

class CheckController extends BaseController 
{

    // public function behaviors()
    // {
    //     return [
    //         'verbs' => [
    //             'class' => VerbFilter::className(),
    //             'actions' => [
    //                 'ajax' => ['post'],
    //             ],
    //         ],
    //         'access' => [
    //             'class' => AccessControl::className(),
    //             'only' => ['index', 'ajax', 'ajax-custom'],
    //             'rules' => [
    //                 [
    //                     'allow' => true,
    //                     'actions' => ['index', 'ajax', 'ajax-custom'],
    //                     'roles' => [ Users::ROLE_ADMIN, Users::ROLE_DISPATCHER, Users::ROLE_REMZONA ],
    //                 ]
    //             ],
    //         ],
    //     ];
    // }

    public function runAction($id, $params = [])
	{
	    $params = \yii\helpers\BaseArrayHelper::merge(Yii::$app->getRequest()->getBodyParams(), $params);
	    return parent::runAction($id, $params);
	}

	public function actionIndex()
    {
        return $this->render('index');
    }

	public function actionAjax($id = null)
	{
		if ($id === null) 
		{
			echo ""; return;
		}
		$this->layout = '@app/views/layouts/null';
		$searchModel = new CheckSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $containers = Containers::find()->filterWhere(['like', 'number', '%' . $id . '%',false])->limit(30)->indexBy('id')->all();

        return $this->render('view', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'containers' => $containers,
            'pattern' => $id
        ]);
	}

    public function actionAjaxCustom($number, $reliz_id, $auto_id)
    {
        $this->layout = '@app/views/layouts/null';

        $reliz  = Reliz::find()->where(['id' => $reliz_id])->one();
        $auto   = Autos::find()->where(['id' => $auto_id])->one();

        $searchModel = new CheckSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $containers = Containers::find()->
                filterWhere(['like', 'number', '%' . $number . '%',false])
                ->limit(30)
                ->indexBy('id')
                ->all();
        
        return $this->render('custom', [
            'dataProvider'  => $dataProvider,
            'searchModel'   => $searchModel,
            'containers'    => $containers,
            'reliz'         => $reliz,
            'auto'          => $auto,
            'pattern'       => $number,
        ]);
    }

    public function actionSaveRelizContainer($container_id, $reliz_id, $auto_id, $date_out = null)
    {
        $date_out = ($date_out === null) ? date("Y-m-d") : $date_out;

        $containers = Containers::find()->where(['id' => $container_id])->one();
        $reliz      = Reliz::find()->where(['id' => $reliz_id])->one();
        $auto       = Autos::find()->where(['id' => $auto_id])->one();

        $containers->id_reliz   = $reliz_id;
        $containers->am_id      = $auto_id;
        $containers->am2        = $auto->number;
        $containers->buking     = $reliz->buking;
        $containers->date_out   = $date_out;
        $containers->reliz      = $reliz->number;
        $containers->operation  = 1;
        $containers->save();

        $rest           = $reliz->rest - 1;
        $reliz->rest    = $rest;
        $reliz->status  = ($rest == 0)? 2 : 0;
        $reliz->save();
    }

}