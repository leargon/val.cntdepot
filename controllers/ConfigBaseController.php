<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use yii\web\View;

class ConfigBaseController extends BaseController
{

    public function beforeAction($action)
    {
        $this->view->params['breadcrumbs'][] = ['label' => 'Настройки', 'url' => ['config/index']];

        if (parent::beforeAction($action)) 
        {
            Yii::$app->view->on(View::EVENT_BEGIN_BODY, function () {});
            return true;
        } else {
            return false;
        }
    }

}