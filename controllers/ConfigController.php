<?php

namespace app\controllers;

use Yii;

use yii\web\Controller;
use yii\web\Response;
use yii\web\NotFoundHttpException;
use yii\helpers\BaseArrayHelper;
use yii\filters\VerbFilter;
use app\models\Containers;
use app\models\ContainersSearch;
use yii\filters\AccessControl;
use app\models\Users;
use app\models\UploadForm;
use yii\web\UploadedFile;

class ConfigController extends BaseController 
{

    // public function behaviors()
    // {
    //     return [
    //         'verbs' => [
    //             'class' => VerbFilter::className(),
    //             'actions' => [
    //                 'ajax' => ['post'],
    //             ],
    //         ],
    //         'access' => [
    //             'class' => AccessControl::className(),
    //             'only' => ['index', 'stock', 'import', 'import_xls'],
    //             'rules' => [
    //                 [
    //                     'allow' => true,
    //                     'actions' => ['index', 'stock', 'import', 'import_xls'],
    //                     'roles' => [ Users::ROLE_ADMIN ],
    //                 ]
    //             ],
    //         ],
    //     ];
    // }

    public function runAction($id, $params = [])
	{
        $body_params = Yii::$app->getRequest()->getBodyParams();
	    $params = BaseArrayHelper::merge($body_params, $params);
	    return parent::runAction($id, $params);
	}

	public function actionIndex()
    {
        return $this->render('index');
    }


    public function actionImport()
    {
        $model = new UploadForm();

        if (Yii::$app->request->isPost) 
        {
            $model->import_in = UploadedFile::getInstance($model, 'import_in');
            $model->import_out = UploadedFile::getInstance($model, 'import_out');

            if ($model->validate()) 
            {
                if ( $model->import_in !== NULL ) 
                {                 
                    $model->import_in->saveAs('uploads/' . $model->import_in->baseName . '.' . $model->import_in->extension);
                    $model->importIn($model->import_in->baseName . '.' . $model->import_in->extension);
                }
                if ( $model->import_out !== NULL)
                {
                    $model->import_out->saveAs('uploads/' . $model->import_out->baseName . '.' . $model->import_out->extension);
                    $model->importOut($model->import_out->baseName . '.' . $model->import_out->extension);
                }
            }
        }

        return $this->render('import', ['model' => $model]);
 
    }

    public function actionImport_xls()
    {
        return $this->render('import_xls');
    }

}