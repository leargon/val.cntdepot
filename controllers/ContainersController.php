<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use app\models\Containers;
use app\models\ContainersSearch;
use app\models\Calc;
use app\models\Stock;
use app\models\Users;
use app\models\Logger;
use app\models\Contracts;
use app\components\MailComponent;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\helpers\VarDumper;
use yii\web\ForbiddenHttpException;
use yii\data\Sort;

class ContainersController extends BaseController
{

    // public function behaviors()
    // {
    //     return [
    //         'verbs' => [
    //             'class' => VerbFilter::className(),
    //             'actions' => [
    //                 'delete' => ['POST','GET'],
    //             ],
    //         ],
    //         'access' => [
    //             'class' => AccessControl::className(),
    //             'only' => [
    //                 'index', 
    //                 'view', 
    //                 'create', 
    //                 'delete', 
    //                 'update', 
    //                 'all', 
    //                 'export', 
    //                 'exportall',
    //                 'shipping',
    //                 'block',
    //                 'contracts',
    //                 'contracts-ajax',
    //                 'contracts-ajax-repair',
    //                 'save-contracts',
    //                 'info',
    //                 'list',
    //                 'calc-export'  
    //             ],
    //             'rules' => [
    //                  [
    //                     'allow' => true,
    //                     'actions' => [
    //                         'index', 
    //                         'view', 
    //                         'create', 
    //                         'delete', 
    //                         'update', 
    //                         'all', 
    //                         'export',
    //                         'exportall', 
    //                         'shipping',
    //                         'exportall',
    //                         'block',
    //                         'contracts',
    //                         'contracts-ajax',
    //                         'contracts-ajax-repair',
    //                         'save-contracts',
    //                         'info',
    //                         'list',
    //                         'calc-export'
    //                     ],
    //                     'roles' => [ Users::ROLE_ADMIN ],
    //                 ],
    //                 [
    //                     'allow' => true,
    //                     'actions' => [
    //                         'index', 
    //                         'view', 
    //                         'create', 
    //                         'update', 
    //                         'all', 
    //                         'export', 
    //                         'exportall',
    //                         'shipping',
    //                         'block',
    //                         'list',
    //                         'calc-export'
    //                     ],
    //                     'roles' => [ Users::ROLE_DISPATCHER ],
    //                 ],
    //                 [
    //                     'allow' => true,
    //                     'actions' => [
    //                         'index', 
    //                         'view', 
    //                         'create',
    //                         'update',
    //                         'delete', 
    //                         'all', 
    //                         'export', 
    //                         'exportall',
    //                         'shipping',
    //                         'block',
    //                         'list',
    //                         'calc-export'
    //                     ],
    //                     'roles' => [ Users::ROLE_CLIENT ],
    //                 ],
    //                 [
    //                     'allow' => true,
    //                     'actions' => [
    //                         'index', 
    //                         'view', 
    //                         'all', 
    //                         'export', 
    //                         'exportall',
    //                         'shipping',
    //                         'block',
    //                         'list',
    //                         'calc-export'
    //                     ],
    //                     'roles' => [ Users::ROLE_REMZONA ],
    //                 ],
    //                 [
    //                     'allow' => true,
    //                     'actions' => [
    //                         'index'
    //                     ],
    //                     'roles' => ['createContainer']
    //                 ]
    //             ],
    //         ],
    //     ];
    // }
    

    // для тестирования чтения письма
    public function actionTest()
    {
        libxml_use_internal_errors(true);

        $config = Yii::$app->params['mail'];

        $inbox = imap_open($config['hostname'], $config['username'], $config['password']) 
            or die('Cannot connect to email: ' . imap_last_error());

        $emails = imap_search($inbox, 'SUBJECT "ESTIMATE" UNSEEN');

        if ($emails) {
            foreach ($emails as $email_number) {
                $header = imap_headerinfo($inbox, $email_number);
                $message = imap_fetchbody($inbox, $email_number, 1);
                $subject = explode(' ', $header->subject);

                // Находим номер контейнера
                $number_reg = preg_match('/[A-Za-z]{4}[0-9]+/', $message, $number);

                // Находим код магазина
                //$shop_code_reg = preg_match('/SHOP CODE: [A-Z0-9]{3}/', $message, $shop_code);


                $number[0] = 'MSKU2422510';

                // находим контейнер по номеру
                $customer = Containers::find()
                ->where(['number' => $number[0]])
                ->one();

                // Находим смету по id контейнера
                $contract = Calc::find()
                ->where(['container_id' => $customer->id])
                ->one();

                // Присваиваем полю error(надо создать) в таблице Calc цифровое значение ошибки(создать массив с ошибками их обозначениями)
                $contract->error_code = 1;
                if (in_array('ERROR', $subject)) {
                    $error_text = explode("\n\r", $message);
                    $contract->error_code = 2;
                    $contract->error_text = $error_text[0];
                }
                
                $contract->save();

                // Если существует $cnt_number1 (номер контейнера) и номер магазина соответствует prime ($shop_res)
                // ТО извлекаем информацию о смете (calc) по его номеру и добавляем код ошибки в новое поле error
                // при выводе контейнеров на странице со всеми контейнерами извелкаем информацию о смете по id контейнера(done)
                // после этого применяем стили к записи контейнера в зависимости от кода ошибки rowOptions
            }
        }

        imap_close($inbox);
    }

    public function actionIndex()
    {   // Сохраняем текущий урл
        // $session = Yii::$app->session;
        // $_SESSION['returnURL'] = Yii::$app->request->url;
        //Url::remember();
        $searchModel = new ContainersSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        // все записи из запроса
        $ids = $dataProvider->query->all();
        $ids = ArrayHelper::getColumn($ids, 'id');

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'ids' => $ids,
        ]);
    }

    public function actionAll()
    {
        $searchModel = new ContainersSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $dataProvider->query->andWhere(['operation' => 1]);
        $dataProvider->pagination->pageSize = 50;

        $sort = $dataProvider->getSort();
        $sort->defaultOrder = ['date_out' => SORT_DESC];
        $dataProvider->setSort($sort);

        $stock = Stock::find()->where(['uid' => Yii::$app->user->identity->id])->one();
        if ($stock != false)
        {
            $dataProvider->query->andWhere(['stock' => $stock->id]);
        }

        return $this->render('all', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionExport()
    {
        $ids = Yii::$app->request->post('ids');
        $ids = explode(',', $ids);
        $fields = [
            'id',
            'number',
            'type',
            'date_in',
            'am_number',
            'statusText',
            'condText',
            'buking',
            'reliz',
            'am2',
            'stock_r.name',
            'date_repair',
            'note',
            'calc.total',
        ];
        $stock = Stock::find()->where(['uid' => Yii::$app->user->identity->id])->one();
        if ($stock != false)
        {
            $containers = Containers::find()
                ->joinWith('calc')
                ->where([ 'operation' => 0, 'stock' => $stock->id ])
                ->andWhere(['containers.id' => $ids])
                ->orderBy(['id' => SORT_DESC]);
        } else {
            $containers = Containers::find()
                ->joinWith('calc')
                ->where([ 'operation' => 0])
                ->andWhere(['containers.id' => $ids])
                ->orderBy(['id' => SORT_DESC]);
        }
        
        $this->createXLS($fields, $containers);

        return $this->redirect(['index']);
        //print_r($ids);
        //VarDumper::dump($ids);
    }

    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }


    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) 
        {
            $check_date = (strtotime($model->date_in) < strtotime(date('Y-m-d', time())));
            $model->time_in = $check_date ? '23:58' : date('H:i', time());
            $model->save();

            // $url = Url::previous();
            // $absoluteHomeUrl = Url::home(true);
            // $url = $absoluteHomeUrl.$url;
            MailComponent::editContainer($model);
            //return $this->redirect([$url]);
            //return $this->redirect(['index']);
            return $this->goBack();
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    public function actionShipping($id)
    {
        $model = $this->findModel($id);

        if($model->load(Yii::$app->request->post()))
        {
            $model->block = 1;
            $model->operation = 1;
            $model->setLog('relized','Отгрузка контейнера:' . $model->number);
            $model->save();

            MailComponent::shipContainer($model);
            return $this->redirect(['index']);
        }
        return $this->render('shipping', [
            'model' => $model,
        ]);
    }

    public function actionAdd()
    {
        $model = new Containers();
        $model->block = 1;
        $user = Yii::$app->getUser()->identity->role;

        if ($model->load(Yii::$app->request->post())) 
        {   

            $check_number = Containers::find()->where(['number' => $model->number])->exists();
            $check_days = (strtotime(date('Y-m-d', time())) - strtotime($model->date_in)) / 60 / 60 / 24;
            $check_date = (strtotime($model->date_in) < strtotime(date('Y-m-d', time())));
            $model->time_in = $check_date ? '23:58' : date('H:i', time());
            //$days = $check_days > 0 ? 'в течении '. $check_days . ' дней' : 'сейчас';
            
            if (!$check_number || ($check_number && $user == 3) || ($user != 3 && $check_days >= 7)) {   
                $model->save();
                MailComponent::editContainer($model);
                return $this->redirect(['index']);                
            } else {
                Yii::$app->session->setFlash('error', "Ошибка, создание контейнера с таким номером в течении 7 дней после приема невозможно!");
            }
   
        }

        return $this->render('add', [
            'model' => $model,
        ]);
    }

    public function actionBlock($id)
    {
        $model = $this->findModel($id);

        if ($model !== null)
        {   
            $model->setLogText('blockoff','Снятие блока с контейнера '.$model->number,'Контейнер с ID#' . $model->id . ' был успешно удален');
            $model->block = 0;
            $model->save();
        }

        return $this->redirect(['index']);
    }

    public function actionContracts($id)
    {
        $contract = Calc::find()->where([ 'container_id' => $id ])->orderBy('id DESC')->one();
        return $this->render('contracts', [
            'model' => $this->findModel($id),
            'calc' => $contract,
        ]);
    }

    private function formate($val, $nums, $int = false) {
	 	$val = (string)$val;
	 
		if (preg_match('/(\.|,)(\d+)/', $val, $match)) {
			if (strlen($match[2]) == 1) {
				$val .= '0';
			}
		} else if (!$int) {
			$val .= '.00';
		}
		
		$val = str_replace('.', '', $val);
		$val = str_replace(',', '', $val);
		
		
		$len = strlen($val);
		$need = $nums - $len;
		if ($need > 0)
		    $res = str_repeat('0', $need);
		else
		    $res = 0;
				
		$res .= $val;
		return $res;
	}
	
    public function actionSaveContracts()
    {
        $post = Yii::$app->request->post();

        $calc_repair_date = !isset($post['current-date']) ? null : $post['repair-date'];

        $container = Containers::find()->where([ 'id' => $post['container_id'] ])->one();
        if ($container == null) 
            exit;

        $container->date_repair = $post['repair_date'];
        $container->save();

        $calc = new Calc;
        if ($calc_repair_date !== null) 
        {
            $calc->date_add = $calc_repair_date;
        }
        $calc->container_id = $post['container_id'];
        $calc->html = $post['table'];
        require('/home/sys78/cntdepot.ru/docs/inc/phpQuery.php');
        $html = \phpQuery::newDocument(stripslashes(htmlspecialchars_decode($calc->html)));
        $calc->total = $html->find('#full_price')->html();
        foreach ($html->find('tr') as $tr) {
            if (pq($tr)->attr('class') != '') continue;
            $allPrice = pq($tr)->find('td')->eq(7)->html();
            // $allPrice = bcdiv($allPrice, 1, 2);
            $allPrice = round($allPrice, 2);
            $allPrice = pq($tr)->find('td')->eq(7)->html($allPrice);
        }
        
        if($calc->save())
        {
            $mail = Yii::$app->mailer->compose();

            $mail->setTo(['mercworkorder@edi.maersk.com','tech@cntdepot.ru','matveev@cntdepot.ru', 'theakak1@gmail.com']);
            $mail->setBcc('edi@depot.solutions');
            $mail->setFrom('edi@depot.solutions');

            $mail->setSubject('Repair');
            
            $workList = '';
		    $work = 'RPRPM  {repair_code}{loc_code}{count}{price}{hours}O';
            // require('/home/sys78/cntdepot.ru/docs/inc/phpQuery.php');
            // $html = \phpQuery::newDocument(stripslashes(htmlspecialchars_decode($calc->html)));
			
    		$hours_all = 0;
    		$price_all = 0;
    		foreach($html->find('tr') as $tr) {
    			if (pq($tr)->attr('class') != '') continue;
    			
    			$d = array(
    				'repair_code' => pq($tr)->find('td')->eq(3)->html(),
    				'loc_code' => pq($tr)->find('td')->eq(4)->html(),
    				'count' => pq($tr)->find('td')->eq(1)->html(),//format 3
    				'price' => pq($tr)->find('td')->eq(5)->html(), //format 12
    				'hours' => pq($tr)->find('td')->eq(6)->html(), //format 4
    			);
    			
    			$p = pq($tr)->find('td')->eq(7)->html();
    					
    			
    			$hours_all += ((int)$d['count'] * $d['hours']);
    			$price_all += $p;
    						
    			$d['count'] = $this->formate($d['count'], 3, true);
    			$d['price'] = $this->formate($d['price'], 12);
    			$d['hours'] = $this->formate($d['hours'], 4);
    				
    			
    			$a = $work;
    			foreach($d as $k => $v) {
    				$a = str_replace('{'.$k.'}', $v, $a);
    			}
    			$worklist .= $a.PHP_EOL;
    		}
    		
    	
    		$data['counter'] = rand();/*(int) file_get_contents($_SERVER['DOCUMENT_ROOT'].'/inc/counter');
    		$data['counter']++;
    		if ($data['counter'] > 999999) {
    			$data['counter'] = 1;
    		}*/
    		
    		$counter = $data['counter'];
    		$data['counter'] = $this->formate($data['counter'], 6, true);
    				
    		$data['date'] = date('dmY');
    		$data['container_number'] = $calc->containers->number;
    		$data['container_type'] = $calc->containers->type_r->type == 'DRY' ? '03' : '02';
    		$data['id'] = $calc->id;
    		$data['hours_all'] = $this->formate($hours_all, 4);
    		$data['price_all'] = $this->formate($price_all, 12);
    		$data['spaces'] = str_repeat(' ', 36 - strlen((string)$data['id']));
            
            $text = 'CTLJ31{counter}
HD1MAERJ31{date}{container_number}{container_type}1   W
HD2{spaces}{id}{hours_all}000000000000{price_all}';

            $text = trim($text);
    				
    		foreach($data as $k => $v) {
    			$text = str_replace('{'.$k.'}', $v, $text);
    		}
    		$text .= PHP_EOL . $worklist;		
    		
    		$text = trim($text);
    		
    		$lines = explode(PHP_EOL, $text);
    		$text = '';
    		foreach($lines as $line) {
    			$len = strlen($line);
    // 			if(80 - $len > 0)
    			$text .= $line.str_repeat(' ', 80 - $len)."\r\n";
    		}

            $mail->attachContent($text, ['fileName' => 'calc'. $calc->id .'.txt', 'contentType' => 'text/plain']);

            $mail->send();
        }
        exit;
    }

    private function formatAjax($id, $selected=true, $repair = false)
    {
        $repair_class = $repair ? ' repair-class ' : '';
        $select = $selected ? ' contract-selected' : '';
        $c = $this->findContractModel($id);
        $r = '<li class="contract-list' . $select . $repair_class . '" onclick="editContract($(this))" ';
        $r .= 'data-repaircode="' . $c->repair_code . '"';
        $r .= 'data-loccode="' . $c->loc_code . '"';
        $r .= 'data-currentcount="1"';
        $r .= 'data-currentprice="' . $c->price . '"';
        $r .= 'data-maxcount="' . $c->max_count . '"';
        $r .= 'data-price="' . $c->price . '"';
        $r .= 'data-hours="' . $c->hours . '"';
        $r .= 'data-element="' . $c->element . '"';
        $r .= 'data-location="' . $c->location . '">';
        $r .= $c->location . ' / ';
        $r .= $c->element . ' / ';
        $r .= $c->repair_type;
        $r .= '</li>';
        return $r;
    }

    public function actionContractsAjax($id)
    {
        echo $this->formatAjax($id);
        exit;
    }

    public function actionContractsAjaxRepair($hours)
    {
        $prepare_code = Contracts::REPAIR_CODE;
        if ($hours > 2) $prepare_code = '0005';
        if ($hours > 8) $prepare_code = '0008';
        if ($hours > 24) $prepare_code = '0011';
        if ($hours > 40) $prepare_code = '0014';
        $repair = Contracts::getByRepair($prepare_code);
        $prepare = Contracts::getByRepair(Contracts::PREPARE_CODE);
        echo $repair !== null ? $this->formatAjax($repair->id, false, true) : '';
        echo $prepare !== null ? $this->formatAjax($prepare->id, false, true) : '';
        exit;
    }

    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $number = $model->number;
        //$model->delete();
        $model->status = 2;
        $model->note = 'Удалено';
        $model->save();

        if ($calc = Calc::find()->where([ 'container_id' => $model->id])->one()) {
            $calc->total = 0;
            $calc->save();
        }

        Logger::addLog(
            'containers', 
            'remove', 
            'Удален ' . $model->number, 
            'Контейнер с ID#' . $model->id . ' был успешно удален'
        );
        return $this->redirect(['index']);
    }

    public function actionExportall()
    {
        $fields = [
            'id',
            'number',
            'type',
            'date_in',
            'am_number',
            'statusText',
            'condText',
            'buking',
            'reliz',
            'am2',
            'stock_r.name',
            'date_repair',
            'note',
        ];
        $containers = Containers::find()
                    ->where([])
                    ->orderBy(['id' => SORT_DESC])
                    ->limit(100);
        $this->createXLS($fields, $containers);
        return $this->redirect(['all']);
    }

    public function actionList()
    {
        $list = Containers::getCountByStocks();
        return $this->render('list', [
            'list' => $list,
        ]);
    }

    private function createXLS($fields ,$query)
    {

        $file = \Yii::createObject([

            'class' => 'codemix\excelexport\ExcelFile',

            'writerClass' => '\PHPExcel_Writer_Excel5', 
            'sheets' => [

                'Active Users' => [
                    'class' => 'codemix\excelexport\ActiveExcelSheet',
                    'query' => $query,
                    'attributes' => $fields,
                ]

            ],
        ]);
        $title = date('d-m-Y', time());
        $file->send( $title . '.xls' );
    }

    /**
     *
     */
    protected function findModel($id)
    {
        if (($model = Containers::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    /**
     *
     */
    protected function findContractModel($id)
    {
        if (($model = Contracts::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    /**
     *
     */

    public function actionCalcExport()
    {
        require('/home/sys78/cntdepot.ru/docs/inc/phpQuery.php');
        $calcs = Calc::find()->all();
        

        foreach ($calcs as $calc) {
            $html = \phpQuery::newDocument(stripslashes(htmlspecialchars_decode($calc->html)));

            if (!$html->find('#full_price')->html()) {
                $calc->total = $html->find('.thead:last > td:eq(2)')->html();
            } else $calc->total = $html->find('#full_price')->html();

            //$calc->total = $html->find('#full_price')->html();
            //$calc->total = $html->find('.thead:last > td:eq(2)')->html();
            $calc->save();
        }

        return;
        //return $this->redirect(['index']);
    }

    // public function beforeAction($action)
    // {
    //     if (parent::beforeAction($action)) {
    //         if (!\Yii::$app->user->can($action->id)) {
    //             throw new ForbiddenHttpException('Access denied');
    //         }
    //         return true;
    //     } else {
    //         return false;
    //     }
    // }
}
