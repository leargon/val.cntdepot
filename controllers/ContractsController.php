<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use app\models\Users;
use app\models\Calc;
use app\models\CalcSearch;
use app\models\Contracts;
use app\models\ContractsSearch;
use app\models\Containers;


class ContractsController extends ConfigBaseController
{

    // public function behaviors()
    // {
    //     return [
    //         'verbs' => [
    //             'class' => VerbFilter::className(),
    //             'actions' => [
    //                 'delete' => ['POST'],
    //             ],
    //         ],
    //         'access' => [
    //             'class' => AccessControl::className(),
    //             'only' => ['index', 'info', 'view', 'create', 'update', 'delete'],
    //             'rules' => [ 
    //                 [
    //                     'allow' => true,
    //                     'actions' => ['index', 'info', 'view', 'create', 'update', 'delete'],
    //                     'roles' => [ Users::ROLE_ADMIN ],
    //                 ] 
    //             ],
    //         ],
    //     ];
    // }
    public function actionIndex($org = null)
    {   
        if ($org == null) {
            return $this->render('index', [
                'dataProvider' => false,
                'org' => isset($org) ? $org : 1 
            ]);
        }
        $searchModel = new ContractsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andWhere(['org' => $org]);
        $sort = $dataProvider->getSort();
        $sort->defaultOrder = ['id' => SORT_DESC];
        $dataProvider->setSort($sort);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'org' => isset($org) ? $org : 1 
        ]);
    }

    public function actionInfo($id)
    {
        $container = Containers::find()->where(['id' => $id])->one();
        if($container === null)
            return $this->redirect(['containers/index']);

        $contract = Calc::find()->where( ['container_id' => $id ] )->orderBy('id DESC')->one();
        require('/home/sys78/cntdepot.ru/docs/inc/phpQuery.php');
        $contract->html = \phpQuery::newDocument(stripslashes(htmlspecialchars_decode($contract->html)));
        foreach ($contract->html->find('tr') as $tr) {
            if (pq($tr)->attr('class') != '') continue;
            $allPrice = pq($tr)->find('td')->eq(7)->html();
            $allPrice = round($allPrice, 2);
            $allPrice = pq($tr)->find('td')->eq(7)->html($allPrice);
        }
        if($contract === null)
            return $this->redirect(['containers/index']);

        return $this->render('info', [
            'container' => $container,
            'contract'  => $contract
        ]);
    }

    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    public function actionCreate()
    {
        $model = new Contracts();

        if ($model->load(Yii::$app->request->post()) && $model->save()) 
        {
            return $this->redirect(['index']);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) 
        {
            return $this->redirect(['index']);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    public function actionDelete($id)
    {
        $calc = Calc::findOne( ['id'=>$id] );
        if ($calc !== null)
        {
            $container = Containers::findOne([ 'id' => $calc->container_id ]);
            $container->date_repair = null;
            $container->save();
            $calc->delete();
        }

        return $this->redirect(['containers/index']);
    }

    protected function findModel($id)
    {
        if (($model = Contracts::findOne($id)) !== null) 
        {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
