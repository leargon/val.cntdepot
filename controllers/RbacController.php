<?php

namespace app\controllers;
use Yii;
use yii\web\Controller;
use yii\data\ArrayDataProvider;
use app\models\RoleForm;
use yii\helpers\ArrayHelper;

class RbacController extends \yii\web\Controller
{
    public function actionCreateRole()
    {
        $role = new RoleForm;
        $auth = Yii::$app->authManager;
        echo "<br><br><br><br><br><br><br><br>";
        //var_dump($auth->getPermissions());
        if($role->load(\Yii::$app->request->post())){
            $roles = $auth->createRole(\Yii::$app->request->post('RoleForm')['name']);
            $auth->add($roles);
            foreach (\Yii::$app->request->post()['RoleForm']['permission'] as $permission) {
                $permission = $auth->getPermission($permission);
                $auth->addChild($roles, $permission);
            }
            return $this->redirect(['index']);
            //var_dump(\Yii::$app->request->post()['RoleForm']['permission']);
        }
        return $this->render('create-role', compact('role'));
    }

    public function actionDelete($id)
    {
        $auth = Yii::$app->authManager;
        $role = $auth->getRole($id);
        $auth->remove($role);
        return $this->redirect(['index']);
    }

    public function actionUpdate($id)
    {
        $role = RoleForm::getRoleByName($id);
       // echo "<br><br><br><br><br><br><br><br>";
        //var_dump($role);
        //$auth = Yii::$app->authManager;
        //$roles = $auth->getRole($id);
        // $role->name = 'nameone';
        // $auth->update($id, $role);
        // return $this->redirect(['index']);
        return $this->render('edit-role', ['role' => $role]);
    }

    public function actionIndex()
    {
        $provider = new ArrayDataProvider([
            'allModels' => Yii::$app->authManager->getRoles(),
        ]);
        return $this->render('index',[
            'provider' => $provider,
        ]);
    }

}
