<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use app\models\Users;
use app\models\Reliz;
use app\models\RelizSearch;
use app\models\Logger;
use app\models\Autos;
use app\models\Stock;
use app\models\Containers;
use app\models\ContainersSearch;


class RelizController extends BaseController
{

    // public function behaviors()
    // {
    //     return [
    //         'verbs' => [
    //             'class' => VerbFilter::className(),
    //             'actions' => [
    //                 'delete' => ['POST'],
    //             ],
    //         ],
    //         'access' => [
    //             'class' => AccessControl::className(),
    //             'only' => ['index', 'view', 'create', 'delete', 'update', 'sel', 'reliz','set-cnt'],
    //             'rules' => [
    //                 [
    //                     'allow' => false,
    //                     'actions' => ['index'],
    //                     'roles' => ['?'],
    //                 ],
    //                 [
    //                     'allow' => true,
    //                     'actions' => ['index', 'view', 'create', 'delete', 'update', 'sel', 'reliz','set-cnt'],
    //                     'roles' => [ Users::ROLE_ADMIN ],
    //                 ],
    //                 [
    //                     'allow' => true,
    //                     'actions' => ['index', 'view', 'create', 'update', 'sel'],
    //                     'roles' => [ Users::ROLE_DISPATCHER ],
    //                 ],
    //                 [
    //                     'allow' => true,
    //                     'actions' => ['index', 'view', 'delete', 'sel'],
    //                     'roles' => [ Users::ROLE_CLIENT ],
    //                 ],
    //                 [
    //                     'allow' => true,
    //                     'actions' => ['index', 'view', 'sel'],
    //                     'roles' => [ Users::ROLE_REMZONA ],
    //                 ]
    //             ],
    //         ],
    //     ];
    // }

    public function actionIndex()
    {        
        $searchModel = new RelizSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $dataProvider->query->andFilterWhere([ '>', 'rest', 0 ]);
        $dataProvider->query->andFilterWhere([ '<', 'status', 3 ]);
        // $dataProvider->query->andWhere('date <= NOW()');
        // $dataProvider->query->andWhere('date_to > NOW()');

        $stock = Stock::find()->where(['uid' => Yii::$app->user->identity->id])->one();
        if ($stock != false)
        {
            $dataProvider->query->andWhere(['stock' => $stock->name]);
        }

        $dataProvider->query->andWhere('(SELECT count(*) FROM containers WHERE containers.reliz = reliz.number) < amount');
        $dataProvider->query->groupBy('number');

        $dataProvider->pagination->pageSize = 50;

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionSetCnt($reliz_id, $auto_id)
    {
        $this->layout = 'null';

        if ($auto_id === null)
            return;

        $reliz  = Reliz::find()->where(['id' => $reliz_id])->one();
        $auto   = Autos::find()->where(['id' => $auto_id])->one();

        $containers = Containers::find()
            ->where(['<', 'status', 3])
            ->andWhere(['type' => $reliz->contType->id])
            ->andWhere(['operation' => 0])
            ->andWhere(['block' => 0])
            ->andWhere(['buking' => ''])
            ->andWhere(['<>', 'conditions', 2])
            ->andWhere(['stock' => $reliz->stock_r->id])
            ->all();

        return $this->render('_set_cnt', [
            'auto'          => $auto,
            'reliz'         => $reliz,
            'containers'    => $containers,
        ]);
    }

    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    public function actionReliz($id) 
    {
        $model = $this->findModel($id);
        $autos = Autos::getAmByReliz($id);

        return $this->render('reliz', [
            'model' => $model,
            'autos' => $autos,
        ]);
    }

    public function actionCreate()
    {
        $model = new Reliz();

        if($model->load(Yii::$app->request->post()))
        {
            $model->rest    = $model->amount;
            $model->status  = 0;
            $model->type    = 0;
        
            if ($model->save()) 
            {
                if($model->method == 1)
                {
                    $numbers = Yii::$app->request->post('number');
                    $drivers = Yii::$app->request->post('driver');
                    if (is_array($numbers)) 
                    {
                        foreach($numbers as $k => $number)
                        {
                            if(isset($drivers[$k]))
                            {
                                $am = new Autos;
                                $am->reliz_id = $model->id;
                                $am->number = $number;
                                $am->driver = $drivers[$k];
                                $am->save();
                            }
                        }
                    }
                }

                return $this->redirect(['index']);
            }

        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $autos = Autos::find()->where(['reliz_id'  => $id])->all();

        if ($model->load(Yii::$app->request->post()) && $model->save()) 
        {
            $result = Autos::deleteAll(['reliz_id'  => $model->id]);

            if($model->method == 1)
            {
                $numbers = Yii::$app->request->post('number');
                $drivers = Yii::$app->request->post('driver');
                if (is_array($numbers)) 
                {
                    foreach($numbers as $k => $number)
                    {
                        if(isset($drivers[$k]))
                        {
                            $auto = new Autos;
                            $auto->reliz_id = $model->id;
                            $auto->number = $number;
                            $auto->driver = $drivers[$k];
                            $auto->save();
                        }
                    }
                }
            }

            return $this->redirect(['index']);
        }

        return $this->render('update', [
            'model' => $model,
            'autos' => $autos
        ]);
    }
    
    public function actionSel($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) 
        {
            return $this->redirect(['index']);
        }

        return $this->render('sel', [
            'model' => $model,
        ]);
    }

    public function actionDelete($id)
    {
        $reliz = $this->findModel($id);

        $log_text = '';

        $autos = Autos::findAll(['reliz_id' => $id]);
        $log_text .= PHP_EOL . 'Связные машины:' . PHP_EOL;
        foreach ($autos as $auto)
        {
            $log_text .= $auto->number;
            $log_text .= $auto->driver != '' ? ' - '. $auto->driver : '';
            $log_text .= PHP_EOL;
        }

        $containers = Containers::findAll(['reliz' => $reliz->number]);
        $log_text .= PHP_EOL . 'Связные контейнеры: ' . PHP_EOL;
        foreach ($containers as $container)
        {
            $text .= $container->number . PHP_EOL;
        }

        Autos::deleteAll(['reliz_id' => $id]);
        Containers::deleteAll(['reliz' => $reliz->number]);
        $reliz->delete();

        Logger::addLog(
            'reliz', 
            'remove', 
            'remove reliz ' . $id, 
            'Релиз с ID#' . $reliz->number . ' был успешно удален' . PHP_EOL . $log_text
        );

        return $this->redirect(['index']);
    }

    protected function findModel($id)
    {
        if (($model = Reliz::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

}
