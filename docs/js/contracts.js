function updatePrice(val) {

	var maxPrice = $('.contract-selected').attr("data-currentprice");
	if ( val > maxPrice ) {
		val = maxPrice;
	}
	$('.contract-selected').attr("data-currentprice", val);

}

function updateCount(val) {

	var maxCount = $('.contract-selected').attr("data-maxcount");
	if ( val > maxCount ) {
		val = maxCount;
	}
	$('.contract-selected').attr("data-currentcount", val);

}

function editContract(item) {

	$("li").removeClass("contract-selected");
	$(item).addClass("contract-selected");
			
	var params = "";
	params = "<p>Максимальное количество: " + $(item).attr("data-maxcount") + "</p>";
	params += "<p>Максимальная стоимость материала:" + $(item).attr("data-price") + "</p>";
	params += "<p>Нормочасы за единицу: " + $(item).attr("data-hours") + "</p>";
	params += "<p>Кол-во: <input type='number' onchange='updateCount($(this).val())' value=\"" + $(item).attr("data-currentcount") + "\" max='" + $(item).attr("data-maxcount") + "'/></p>";
	params += "<p>Материалы за шт: <input type='number' onchange='updatePrice($(this).val())' value=\"" + $(item).attr("data-currentprice") + "\" max='" + $(item).attr("data-price") + "'/></p>";
	params += "<p><button onclick='payment();$(\".repair_date\").show();'>Расчитать</button></p>";
	params += "<p class=\"repair_date\" style=\"display:none;\"><input id=\"current\" type=\"checkbox\" name=\"current-date\" checked/>  Дата ремонта: <input class=\"date-input\" value=\"" + current_date + "\"><p><button onclick=\"saveContracts();\">Сохранить</button></p></p>";
			
	$("#contract-params").html(params);

}

function payment() {

	$(".contract-table").show();
	$(".cnt-print").remove();

	var all_price = job_price = 0;
	var full_price = full_job_price = 0;

	$(".contract-list").each(function( i ) {

		all_price = $( this ).attr("data-currentprice") * $( this ).attr("data-currentcount");
		job_price = $( this ).attr("data-hours") * hours_price;
		all_price += job_price;

		full_price 		+= all_price;
		full_job_price 	+= job_price;

		var row = "";
		row += "<tr class=\"cnt-print\">";
		row += "<td>" + ++i + "</td>"
		row += "<td>" + $( this ).attr("data-currentcount") + "</td>"
		row += "<td>" + $( this ).text() + "</td>"
		row += "<td>" + $( this ).attr("data-repaircode") + "</td>"
		row += "<td>" + $( this ).attr("data-loccode") + "</td>"
		row += "<td>" + $( this ).attr("data-currentprice") + "</td>"
		row += "<td>" + $( this ).attr("data-hours") + "</td>"
		row += "<td>" + all_price + "</td>"
		row += "</tr>"

		$(".contract-table tr:last").after(row);

	});

	var row = "";
	row += "<tr class=\"cnt-print thead last-row\">";
	row += "<td colspan=\"4\"></td>";
	row += "<td>Итого</td>";
	row += "<td id=\"full_price\">"+ full_price + "</td>";
	row += "<td id=\"full_job\">" + full_job_price + "</td></tr>";

	$(".contract-table tr:last").after(row);
}

function saveContracts() {

	var row = "<table>";

	row += "<tr class=\"thead\">";
	row += "<td>Номер, пп</td>";
	row += "<td>Кол-во, шт</td>";
	row += "<td>Описание</td>";
	row += "<td>Код ремонта</td>";
	row += "<td>Loc Code</td>";
	row += "<td>Материал за шт.</td>";
	row += "<td>Нормочасов за шт</td>";
	row += "<td>Итого по виду работ</td>";
	row += "</tr>";

	var all_price = job_price = 0; 

	$(".contract-list").each(function( i ) {

		all_price = $( this ).attr("data-currentprice") * $( this ).attr("data-currentcount");
		job_price = $( this ).attr("data-hours") * hours_price;
		all_price += job_price;

		row += "<tr>";
		row += "<td>" + ++i + "</td>"
		row += "<td>" + $( this ).attr("data-currentcount") + "</td>"
		row += "<td>" + $( this ).text() + "</td>"
		row += "<td>" + $( this ).attr("data-repaircode") + "</td>"
		row += "<td>" + $( this ).attr("data-loccode") + "</td>"
		row += "<td>" + $( this ).attr("data-currentprice") + "</td>"
		row += "<td>" + $( this ).attr("data-hours") + "</td>"
		row += "<td>" + all_price + "</td>"
		row += "</tr>"
	});

	row += $(".last-row").html();
	row += "</table>";

	var current_date = $("#current").prop("checked") ? "&current-date=1" : "";
			
	$.ajax({
		url: url_save,
		type: "POST",
		data: "table=" + row + "&repair_date=" + $('.date-input').val() + "&container_id=" + container_id  + current_date,
	}).done(function(data) {
		window.location.href = url_index;
	});

	return false;

}

$( ".location" ).click(function() {
	$(this).next().toggle();
});

$( ".element" ).click(function() {
	$(this).next().toggle();
});

$( ".repair-type" ).click(function() {
	$("li").removeClass("repair-selected");
	$(this).addClass("repair-selected");
});

$( ".contract-list" ).click(function() {
	$("li").removeClass("contract-selected");
	$(this).addClass("contract-selected");
	$("#contract-params").html($(this).text());
});

$( ".contract-add" ).click(function() {
	var id = $(".repair-selected").attr("data-id");

	$.ajax({
		url: url_get_contracts + id,
	}).done(function(data) {
		$("#types-selected").append(data);
	});
});

$( ".contract-del" ).click(function() { 
	$( ".contract-selected" ).remove();
	$( "#contract-params" ).html("");
});