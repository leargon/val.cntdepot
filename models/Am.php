<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "am".
 *
 * @property int $id
 * @property string $number
 * @property string $driver
 * @property int $reliz_id
 */
class Am extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'am';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['number', 'driver', 'reliz_id'], 'required'],
            [['number', 'driver'], 'string'],
            [['reliz_id'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'number' => 'Number',
            'driver' => 'Driver',
            'reliz_id' => 'Reliz ID',
        ];
    }

    public static function getAmByReliz($reliz_id) 
    {
        $result = Am::find()->where(['reliz_id' => $reliz_id])->all();
        // var_dump(count($result), $reliz_id);exit;

        return ($result !== false) ? $result : []; 
    }
}
