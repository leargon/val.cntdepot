<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "auto".
 *
 * @property int $id
 * @property string $number
 * @property string $driver
 * @property int $reliz_id
 */
class Autos extends LogModel
{

    public static function tableName()
    {
        return 'autos';
    }

    public function rules()
    {
        return [
            [['number', 'driver', 'reliz_id'], 'required'],
            [['number', 'driver'], 'string'],
            [['reliz_id'], 'integer'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'id',
            'number' => 'Номер',
            'driver' => 'Водитель',
            'reliz_id' => 'Релиз',
        ];
    }

    public function getReliz()
    {
        return $this->hasOne(Reliz::className(), ['id' => 'reliz_id']);
    }


    public static function getAmByReliz($reliz_id) 
    {
        $result = Autos::find()
            ->where(['reliz_id' => $reliz_id ])
            ->all();
        return ($result !== false) ? $result : []; 
    }

    public function checkContainer()
    {
        if($this->reliz_id === null)
            return false;
        $container = Containers::find()
            ->where([ 'reliz' => $this->reliz->number ])
            ->andWhere([ 'am2' => $this->number ])
            ->one();

        return $container !== null ? true : false;
    }

}
