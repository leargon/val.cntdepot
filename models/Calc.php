<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "calc".
 *
 * @property int $id
 * @property int $container_id
 * @property string $html
 * @property string $date_add
 * @property int $error_code
 */
class Calc extends \yii\db\ActiveRecord
{
    const ERROR_CODE_NO_RESPONSE = 0;
    const ERROR_CODE_SUCCESS_RESPONSE = 1;
    const ERROR_CODE_RESPONSE_WITH_ERROR = 2;

    public static function tableName()
    {
        return 'calc';
    }

    public function rules()
    {
        return [
            [['container_id', 'html'], 'required'],
            [['container_id', 'error_code'], 'integer'],
            [['html'], 'string'],
            [['date_add'], 'safe'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'id',
            'container_id' => 'Идентификатор контейнера',
            'html' => 'Общее описание',
            'date_add' => 'Дата добавления',
            'total' => 'Итого',
            'error_code' => 'Код ошибки'
        ];
    }

    public function getContainers()
    {
        return $this->hasOne(Containers::className(), ['id' => 'container_id']);
    }
}
