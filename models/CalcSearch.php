<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Calc;

/**
 * CalcSearch represents the model behind the search form of `app\models\Calc`.
 */
class CalcSearch extends Calc
{

    public function rules()
    {
        return [
            [['id', 'container_id'], 'integer'],
            [['html', 'date_add'], 'safe'],
        ];
    }

    public function scenarios()
    {
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Calc::find();

        $query->joinWith(['containers']);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'container_id' => $this->container_id,
            'date_add' => $this->date_add,
        ]);

        $query->andFilterWhere(['like', 'html', $this->html]);

        return $dataProvider;
    }
}
