<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Containers;

class CheckSearch extends Containers
{
    public $date_in_s;
    public $date_in_e;
    public $date_out_s;
    public $date_out_e;
    public $date_repair_s;
    public $date_repair_e;
    public $calc_error_code;

    public function rules()
    {
        return [
            [['id', 'conditions', 'status', 'block', 'operation', 'am_id', 'calc_error_code'], 'integer'],
            [
                [
                    'buking',
                    'reliz',
                    'stock',
                    'number',
                    'type',
                    'date_in',
                    'time_in',
                    'date_out',
                    'date_repair',
                    'am_number',
                    'am2',
                    'note',
                    'time_out',
                    'date_in_s',
                    'date_in_e',
                    'date_out_s',
                    'date_out_e',
                    'date_repair_s',
                    'date_repair_e',
                    'calc_error_code',
                ],
                'safe'
            ],
        ];
    }

    public function scenarios()
    {
        return Model::scenarios();
    }

    public function search($params)
    {
        $this->load($params);

        $query = Containers::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'date_in' => $this->date_in,
            'time_in' => $this->time_in,
            'date_out' => $this->date_out,
            'date_repair' => $this->date_repair,
            'conditions' => $this->conditions,
            'status' => $this->status,
            'block' => $this->block,
            'operation' => $this->operation,
            'time_out' => $this->time_out,
            'am_id' => $this->am_id,
        ]);

        $query->andFilterWhere(['like', 'buking', $this->buking])
            ->andFilterWhere(['like', 'reliz', $this->reliz])
            ->andFilterWhere(['like', 'stock', $this->stock])
            ->andFilterWhere(['like', 'number', $this->number])
            ->andFilterWhere(['like', 'type', $this->type])
            ->andFilterWhere(['like', 'am_number', $this->am_number])
            ->andFilterWhere(['like', 'am2', $this->am2])
            ->andFilterWhere(['like', 'note', $this->note]);

        $query->andFilterWhere(['>=', 'date_in', $this->date_in_s]);
        $query->andFilterWhere(['<=', 'date_in', $this->date_in_e]);
        $query->andFilterWhere(['>=', 'date_out', $this->date_out_s]);
        $query->andFilterWhere(['<=', 'date_out', $this->date_out_e]);
        $query->andFilterWhere(['>=', 'date_repair', $this->date_repair_s]);
        $query->andFilterWhere(['<=', 'date_repair', $this->date_repair_e]);

        return $dataProvider;
    }
}
