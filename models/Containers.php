<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "containers".
 *
 * @property int $id
 * @property string $buking
 * @property string $reliz
 * @property string $stock
 * @property string $number
 * @property string $type
 * @property string $date_in
 * @property string $time_in
 * @property string $date_out
 * @property string $date_repair
 * @property string $am_number
 * @property string $am2
 * @property int $conditions
 * @property int $status
 * @property int $block
 * @property string $note
 * @property int $operation
 * @property string $time_out
 * @property int $am_id
 */
class Containers extends LogModel
{

    public $cnt;
    public $price;

    public static function tableName()
    {
        return 'containers';
    }

    public function rules()
    {
        return [
            [['stock', 'number', 'type', 'date_in', 'am_number', 'conditions', 'status', 'block'], 'required'],
            [['am_number','am2','operation','am_id','reliz','date_in', 'date_out', 'date_repair', 'time_out','cnt'], 'safe'],
            [['note'], 'string'],

        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'id',
            'buking' => 'Букинг',
            'reliz' => 'Релиз',
            'stock' => 'Сток',
            'number' => 'Номер конт-ра',
            'type' => 'Тип',
            'date_in' => 'Дата прихода ',
            'time_in' => 'Время прихода',
            'date_out' => 'Дата выдачи',
            'date_repair' => 'Дата ремонта',
            'am_number' => 'Номер АМ',
            'am2' => 'АМ Комп.',
            'conditions' => 'Состояние конт-ра',
            'status' => 'Порожний/ Груженый',
            'block' => 'Блок',
            'note' => 'Примечание',
            'operation' => 'Операции',
            'time_out' => 'Time Out',
            'am_id' => 'Am ID',
            'condText'=>'Состояние',
            'statusText'=>'Статус',
            'blockText'=>'Блок',
            'price'=>'Итого',
            'calc_error_code' => 'Смета'
        ];
    }

    public function beforeSave($insert)
    {
        Yii::$app->cache->flush();
        if (parent::beforeSave($insert)) 
        {
            return true;
        } else {
            return true;
        }
    }

    public function afterDelete()
    {
        Yii::$app->cache->flush();
        parent::afterDelete();
    }

    public function getStock_r()
    {
        return $this->hasOne(Stock::className(), ['id' => 'stock']);
    }

    public function getType_r()
    {
        return $this->hasOne(Types::className(), ['id' => 'type']);
    }

    public function getReliz_r()
    {
        return $this->hasOne(Reliz::className(), ['number' => 'reliz']);
    }

    public static function getStatusArr()
    {
        return [
            'Порожний',
            'Груженный'
        ];
    }

    public function getPrice()
    {
        return 'Итого';
    }

    public function getStatusText()
    {
        $items = Containers::getStatusArr();
        return isset($items[$this->status]) ? $items[$this->status] : '';
    }

    public static function getCondArr()
    {
        return [
            'Исправен',
            'Грязный',
            'Поврежден'
        ];
    }

    public function getCondId($name)
    {
        $items = Containers::getCondArr();
        foreach ($items as $i => $item)
        {
            if ($item === trim($name))
                return $i;
        }
        return 0;
    }

    public function getCondText()
    {
        $items = Containers::getCondArr();
        return isset($items[$this->conditions]) ? $items[$this->conditions] : '';
    }    

    public static function getBlockArr()
    {
        return ['Нет','Да'];
    }

    public function getBlockText()
    {
        $items = Containers::getBlockArr();
        return isset($items[$this->block]) ? $items[$this->block] : '';
    }

    public function getStockId($name)
    {
        $stocks = Stock::find()->all();
        if ($stocks === null || !is_array($stocks))
            return 0;
        foreach ($stocks as $stock)
        {
            if ($stock->name === $name)
                return $stock->id;
        }
        return 0;
    }

    public static function getCountByStocks()
    {
        $list = Containers::find()
            ->select(['COUNT(*) AS cnt','stock'])
            ->groupBy(['stock'])
            ->orderBy('cnt DESC')
            ->all();
        return $list === null ? [] : $list;
    }

    public function getCalc()
    {
        return $this->hasOne(Calc::className(), ['container_id' => 'id'])->orderBy('id DESC');
    }

}
