<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Containers;
use yii\db\ActiveQuery;

class ContainersSearch extends Containers
{
    public $date_in_s;
    public $date_in_e;
    public $date_out_s;
    public $date_out_e;
    public $date_repair_s;
    public $date_repair_e;
    public $calc_error_code;

    public function rules()
    {
        return [
            [['id', 'conditions', 'status', 'block', 'operation', 'am_id', 'calc_error_code'], 'integer'],
            [
                [
                    'buking',
                    'reliz',
                    'stock',
                    'number',
                    'type',
                    'date_in',
                    'time_in',
                    'date_out',
                    'date_repair',
                    'am_number',
                    'am2',
                    'note',
                    'time_out',
                    'date_in_s',
                    'date_in_e',
                    'date_out_s',
                    'date_out_e',
                    'date_repair_s',
                    'date_repair_e',
                    'calc_error_code',
                ],
                'safe'
            ],
        ];
    }

    public function scenarios()
    {
        return Model::scenarios();
    }

    public function search($params)
    {
        $this->load($params);

        $calc = Calc::find()
            ->groupBy('container_id')
            ->orderBy(['id' => SORT_DESC]);

        $query = Containers::find()
            ->leftJoin(['calc' => $calc], 'calc.container_id = containers.id')
        ;

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 50,
            ],
        ]);

        $dataProvider->setSort([
            'defaultOrder' => ['date_in' => SORT_DESC],
            'attributes' => [
                'id',
                'buking',
                'reliz',
                'stock',
                'number',
                'type',
                'date_in',
                'time_in',
                'date_out',
                'date_repair',
                'am_number',
                'am2',
                'note',
                'operation',
                'time_out',
                'am_id',
                'conditions',
                'status',
                'block',
                'calc_error_code' => [
                    'asc' => ['calc.error_code' => SORT_ASC],
                    'desc' => ['calc.error_code' => SORT_DESC]
                ],
            ]
        ]);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'date_in' => $this->date_in,
            'time_in' => $this->time_in,
            'date_out' => $this->date_out,
            'date_repair' => $this->date_repair,
            'conditions' => $this->conditions,
            'status' => $this->status,
            'block' => $this->block,
            'operation' => $this->operation,
            'time_out' => $this->time_out,
            'am_id' => $this->am_id,
        ]);

        $query->andFilterWhere(['like', 'buking', $this->buking])
            ->andFilterWhere(['like', 'reliz', $this->reliz])
            ->andFilterWhere(['like', 'stock', $this->stock])
            ->andFilterWhere(['like', 'number', $this->number])
            ->andFilterWhere(['like', 'type', $this->type])
            ->andFilterWhere(['like', 'am_number', $this->am_number])
            ->andFilterWhere(['like', 'am2', $this->am2])
            ->andFilterWhere(['like', 'note', $this->note]);

        $query->andFilterWhere(['>=', 'date_in', $this->date_in_s]);
        $query->andFilterWhere(['<=', 'date_in', $this->date_in_e]);
        $query->andFilterWhere(['>=', 'date_out', $this->date_out_s]);
        $query->andFilterWhere(['<=', 'date_out', $this->date_out_e]);
        $query->andFilterWhere(['>=', 'date_repair', $this->date_repair_s]);
        $query->andFilterWhere(['<=', 'date_repair', $this->date_repair_e]);

        $stock = Stock::find()->where(['uid' => Yii::$app->user->identity->id])->one();
        if ($stock !== null)
        {
            $query->andWhere(['stock' => $stock->id]);
        }

        $query->andWhere(['operation' => 0]);

        return $dataProvider;
    }
}
