<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "contracts".
 *
 * @property int $id
 * @property string $org
 * @property string $container_type
 * @property string $container_size
 * @property string $location
 * @property string $element
 * @property string $repair_type
 * @property string $repair_code
 * @property string $repair_desc
 * @property string $price
 * @property double $hours
 * @property int $max_count
 * @property string $loc_code
 */
class Contracts extends \yii\db\ActiveRecord
{

    const REPAIR_CODE   = '0003';
    const PREPARE_CODE  = '0917';

    public static function tableName()
    {
        return 'contracts';
    }

    public function rules()
    {
        return [
            [['org', 'container_type', 'container_size', 'location', 'element', 'repair_type', 'repair_code', 'repair_desc', 'price', 'hours', 'max_count', 'loc_code'], 'required'],
            [['max_count'], 'integer'],
            [['price', 'hours'], 'number'],
            [['org', 'container_type', 'container_size', 'location', 'element', 'repair_type', 'repair_code', 'repair_desc', 'loc_code'], 'string', 'max' => 255],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'id',
            'org' => 'Организация',
            'container_type' => 'Тип',
            'container_size' => 'Размер',
            'location' => 'Расположение',
            'element' => 'Элемент',
            'repair_type' => 'Вид ремонта',
            'repair_code' => 'Код',
            'repair_desc' => 'Описание ремонта',
            'price' => 'Стоимость',
            'hours' => 'Нормочасы',
            'max_count' => 'Количество',
            'loc_code' => 'Loc Code',
        ];
    }

    public function beforeSave($insert)
    {
        Yii::$app->cache->flush();
        if (parent::beforeSave($insert)) 
        {
            return true;
        } else {
            return true;
        }
    }

    public function afterDelete()
    {
        Yii::$app->cache->flush();
        parent::afterDelete();
    }

    public function getOrg_r()
    {
        return $this->hasOne(Orgs::className(), ['id' => 'org']);
    }

    public static function getOrg_id($name)
    {

        $org = Orgs::find()->where(['name' => $name])->one();
        return $org != null? $org->id : 0;
    }

    public static function getLocations($org_id, $container_size)
    {  
        // var_dump($org_id ,$container_size);exit;
        $records = Contracts::find()
            ->where(['org' => $org_id, 'container_size' => $container_size])
            ->groupBy(['location'])
            ->all();
        return $records ? $records : [];
    }

    public static function getElements($location, $container_size)
    {
        $records = Contracts::find()
            ->where(['location' => $location, 'container_size' => $container_size])
            ->groupBy(['element'])
            ->all();
        return $records ? $records : [];
    }

    public static function getTypes($element, $container_size,$location='')
    {
		if ($location!='') {
			$records = Contracts::find()
            ->where(['element' => $element, 'container_size' => $container_size,'location'=>$location])
            ->groupBy(['repair_type'])
            ->all();
		} else {
			$records = Contracts::find()
            ->where(['element' => $element, 'container_size' => $container_size])
            ->groupBy(['repair_type'])
            ->all();
        }    
        return $records ? $records : [];
    }

    public static function getByCode($repair_code, $container_size)
    {
        $records = Contracts::find()
            ->where(['repair_code' => $repair_code, 'container_size' => $container_size])
            ->groupBy(['repair_type'])
            ->one();
        return $records ? $records : [];
    }
    
    public static function getByRepair($repair_code)
    {
        $records = Contracts::find()
            ->where(['repair_code' => $repair_code])
            ->groupBy(['repair_type'])
            ->one();
        return $records ? $records : [];
    }

}
