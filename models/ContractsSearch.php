<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Contracts;

class ContractsSearch extends Contracts
{
    
    public function rules()
    {
        return [
            [['id', 'size_from', 'size_to', 'max_count'], 'integer'],
            [['org', 'container_type', 'container_size', 'location', 'element', 'repair_type', 'repair_code', 'repair_desc', 'loc_code'], 'safe'],
            [['price', 'hours'], 'number'],
        ];
    }
    
    public function scenarios()
    {
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = Contracts::find();
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) 
        {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'size_from' => $this->size_from,
            'size_to' => $this->size_to,
            'price' => $this->price,
            'hours' => $this->hours,
            'max_count' => $this->max_count,
        ]);

        $query->andFilterWhere(['like', 'org', $this->org])
            ->andFilterWhere(['like', 'container_type', $this->container_type])
            ->andFilterWhere(['like', 'container_size', $this->container_size])
            ->andFilterWhere(['like', 'location', $this->location])
            ->andFilterWhere(['like', 'element', $this->element])
            ->andFilterWhere(['like', 'repair_type', $this->repair_type])
            ->andFilterWhere(['like', 'repair_code', $this->repair_code])
            ->andFilterWhere(['like', 'repair_desc', $this->repair_desc])
            ->andFilterWhere(['like', 'loc_code', $this->loc_code]);

        return $dataProvider;
    }
}
