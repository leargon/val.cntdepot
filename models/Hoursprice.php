<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "hoursprice".
 *
 * @property int $id
 * @property string $org
 * @property string $container_size
 * @property string $price
 */
class Hoursprice extends \yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'hoursprice';
    }

    public function rules()
    {
        return [
            [['org', 'container_size', 'price'], 'required'],
            [['price'], 'number'],
            [['org', 'container_size'], 'string', 'max' => 255],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'id',
            'org' => 'Организация',
            'container_size' => 'Размер контейнера',
            'price' => 'Стоимость нормочаса',
        ];
    }

    public function getOrg_r()
    {
        return $this->hasOne(Orgs::className(), ['id' => 'org']);
    }

    public static function getContainerSize()
    {
        $records = Hoursprice::find()->all();
        $items = ArrayHelper::map($records,'container_size','container_size');
        return ($items === NULL) ? [] : $items;
    }

    public static function getPrice($size)
    {
        $record = Hoursprice::findOne(['container_size' => $size]);
        return $record != null ? $record->price : 0;
    }

}
