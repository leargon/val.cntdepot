<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Hoursprice;

/**
 * HourspriceSearch represents the model behind the search form of `app\models\Hoursprice`.
 */
class HourspriceSearch extends Hoursprice
{

    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['org', 'container_size'], 'safe'],
            [['price'], 'number'],
        ];
    }

    public function scenarios()
    {
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = Hoursprice::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'price' => $this->price,
        ]);

        $query->andFilterWhere(['like', 'org', $this->org])
            ->andFilterWhere(['like', 'container_size', $this->container_size]);

        return $dataProvider;
    }
}
