<?php

namespace app\models;

use Yii;
use app\models\Logger;

class LogModel extends \yii\db\ActiveRecord
{

	private $log_title = '';
	private $log_note = '';
	private $log_text = '';

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) 
        {
        	$attributes = $this->attributeLabels();
	    	$before 	= $this->oldAttributes;
	    	$log = '';
	    	
	    	if(!empty($this->log_text)) {
		    	Logger::addLog(
				    	$this->tableName(),
				    	$this->log_title,
				    	$this->log_note,
			    		$this->log_text
		    		);
		    	return true;
		    }

			if ($insert)
	    	{
				foreach ($this->getAttributes() as $k => $attr)
	    		{
	    			if (!empty($attr) && $attr !== null)
	    			{
	    				$log .= $attributes[$k] .': ' . $attr . PHP_EOL;
	    			}
	    		}
	    	} else {
	    		foreach ($this->getAttributes() as $k => $attr)
	    		{
	    			if (isset($before[$k]) && isset($attributes[$k]) && $before[$k] != $attr)
	    			{
	    				$log .= $attributes[$k] .': ' . $before[$k] . ' -> ' . $attr . PHP_EOL;
	    			}
	    		}
	    	}

	    	if (!empty($this->log_title) && !empty($this->log_note)) 
	    	{
	    		Logger::addLog(
			    	$this->tableName(),
			    	$this->log_title,
			    	$this->log_note,
		    		$log
	    		);
	    	} else {
	    		Logger::addLog(
			    	$this->tableName(),
			    	$insert ? 'add' : 'edit',
			    	//$insert ? 'Добавление' : 'Изменения в ' . $this->id,
			    	$insert ? 'Добавлен ' . $this->number : 'Изменения в ' . $this->number,
			    	$log
	    		);
	    	}
	    	
	    	return true;
        } else {
	    	return true;
        }

    }

    public function setLog($title, $note)
    {
        $this->log_title = $title;
        $this->log_note	= $note;
    }

    public function setLogText($title, $note, $text)
    {
        $this->log_title = $title;
        $this->log_note	= $note;
        $this->log_text	= $text;
    }
}