<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "logger".
 *
 * @property int $id
 * @property int $user_id
 * @property string $type
 * @property string $action
 * @property string $ip
 * @property string $date_add
 * @property string $note
 * @property string $text
 */
class Logger extends \yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'logger';
    }

    public function rules()
    {
        return [
            [['user_id', 'type', 'action', 'date_add', 'note'], 'required'],
            [['user_id'], 'integer'],
            [['date_add'], 'safe'],
            [['text'], 'string'],
            [['type', 'note'], 'string', 'max' => 255],
            [['action', 'ip'], 'string', 'max' => 50],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'id',
            'user_id' => 'Пользователь',
            'type' => 'Тип',
            'action' => 'Действие',
            'actionText' => 'Действие',
            'ip' => 'ip',
            'date_add' => 'Дата добавления',
            'note' => 'Примечание',
            'text' => 'Текст',
        ];
    }

    public function getUser()
    {
        return $this->hasOne(Users::className(), ['id' => 'user_id']);
    }

    public static function getActionArr()
    {
        return [
            'edit' => 'Редактирование',
            'add' => 'Добавление',
            'remove' => 'Удаление',
            'relized' => 'Отгрузка',
            'blockoff' => 'Снятие блока',
        ];
    }

    public function getActionText()
    {
        $items = Logger::getActionArr();
        return isset($items[$this->action]) ? $items[$this->action] : '';
    }

    public static function getTypeArr()
    {
        return [
            'containers'    => 'Контейнер',
            'reliz'         => 'Релиз',
            'autos'         => 'Водитель',
        ];
    }

    public function getTypeText()
    {
        $items = Logger::getTypeArr();
        return isset($items[$this->type]) ? $items[$this->type] : '';
    }

    public static function addLog($type, $action, $note, $text)
    {
        $log = new Logger;
        if (Yii::$app instanceof \yii\console\Application) 
        {
            $log->user_id = 23;
        } else {
            $log->user_id = Yii::$app->user->identity->id;
        }
        $log->action = $action;
        $log->type = $type;
        $log->note = $note;
        $log->text = $text;
        $log->date_add = date('Y-m-d H:i:s'); 
        $log->ip = isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : '';
        $log->save();
        return;
    }

}
