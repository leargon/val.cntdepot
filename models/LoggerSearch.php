<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Logger;

class LoggerSearch extends Logger
{
    public $date_add_s;
    public $date_add_e;
 
    public function rules()
    {
        return [
            [['id', 'user_id'], 'integer'],
            [['type', 'action', 'ip', 'date_add', 'note', 'text', 'date_add_s', 'date_add_e'], 'safe'],
        ];
    }

    public function scenarios()
    {
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = Logger::find()->orderBy([ 'id' => SORT_DESC]);
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }
        
        $query->andFilterWhere([
            'id' => $this->id,
            'user_id' => $this->user_id,
            'date_add' => $this->date_add,
        ]);

        $query->andFilterWhere(['like', 'type', $this->type])
            ->andFilterWhere(['like', 'action', $this->action])
            ->andFilterWhere(['like', 'ip', $this->ip])
            ->andFilterWhere(['like', 'note', $this->note])
            ->andFilterWhere(['like', 'text', $this->text]);

        $query->andFilterWhere(['>=', 'date_add', $this->date_add_s]);
        $query->andFilterWhere(['<=', 'date_add', $this->date_add_e]);

        return $dataProvider;
    }
}
