<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "messages".
 *
 * @property int $id
 * @property string $to
 * @property string $from
 * @property string $subject
 * @property string $body
 * @property string $create
 * @property string $send
 */
class Messages extends \yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'messages';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['create', 'send'], 'safe'],
            [['to', 'from', 'subject'], 'string', 'max' => 255],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'id',
            'to' => 'Кому',
            'from' => 'От',
            'subject' => 'Тема',
            'body' => 'Текст сообщения',
            'create' => 'Созданно',
            'send' => 'Отправленно',
        ];
    }
}
