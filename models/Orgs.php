<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "orgs".
 *
 * @property int $id
 * @property string $name
 * @property string $email
 * @property string identifier
 */
class Orgs extends \yii\db\ActiveRecord
{

    public static function tableName()
    {
        return 'orgs';
    }

    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 45],
            ['email', 'email'],
            [['email','identifier'], 'safe'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'id',
            'name' => 'Орг',
            'identifier' => 'Идентификатор',
        ];
    }

    public static function getStocks()
    {
        $records = Stock::find()->all();
        $items = ArrayHelper::map($records,'id','name');
        return ($items === NULL) ? [] : $items;
    }

    public static function getOrgs()
    {
        $records = Orgs::find()->all();
        $items = ArrayHelper::map($records,'id','name');
        return ($items === NULL) ? [] : $items;
    }

    public static function getOrgNames()
    {
        $records = Orgs::find()->all();
        $items = ArrayHelper::map($records,'name','name');
        return ($items === NULL) ? [] : $items;
    }

}
