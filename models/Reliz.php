<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "reliz".
 *
 * @property int $id
 * @property string $stock
 * @property string $type_cnt
 * @property int $amount
 * @property int $rest
 * @property string $date
 * @property string $date_to
 * @property string $number
 * @property string $buking
 * @property int $method
 * @property int $type
 * @property int $status
 * @property string $comment
 */
class Reliz extends LogModel
{
    public $sum;

    public static function tableName()
    {
        return 'reliz';
    }

    public function rules()
    {
        return [
            [['stock', 'type_cnt', 'amount', 'date', 'number', 'buking', 'method'], 'required'],
            [['amount', 'rest', 'method', 'type', 'status'], 'integer'],
            [['date', 'date_to', 'sum'], 'safe'],
            [['comment'], 'string'],
            [['stock', 'type_cnt', 'number', 'buking'], 'string', 'max' => 255],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'id',
            'stock' => 'Сток',
            'type_cnt' => 'Тип',
            'amount' => 'Кол-во',
            'rest' => 'Остаток',
            'restCount' => 'Остаток',
            'date' => 'Дата выдачи',
            'date_to' => 'Дата выгрузки по',
            'formated_dates' => 'Дата выдачи',
            'number' => 'Релиз',
            'buking' => 'Букинг',
            'method' => 'Способ отгрузки',
            'type' => 'Тип',
            'status' => 'Статус',
            'comment' => 'Комментарий',
            'countByReliz' => 'Остаток конт.'
        ];
    }

    public function beforeSave($insert)
    {
        Yii::$app->cache->flush();
        if (parent::beforeSave($insert)) 
        {
            return true;
        } else {
            return true;
        }
    }

    public function afterDelete()
    {
        Yii::$app->cache->flush();
        parent::afterDelete();
    }

    public function getLabel($attribute)
    {
        $labels = $this->attributeLabels();
        return isset($labels[$attribute]) ? $labels[$attribute] : '';
    }

    public function getContType()
    {
        return $this->hasOne(Types::className(), ['name' => 'type_cnt']);
    }

    public function getStock_r()
    {
        return $this->hasOne(Stock::className(), ['name' => 'stock']);
    }

    public function getAutos()
    {
        return $this->hasMany(Autos::className(), ['reliz_id' => 'id']);
    }

    public function getContainers()
    {
        return $this->hasMany(Containers::className(), ['reliz' => 'number']);
    }

    public function getFormatedDates()
    {
        return $this->date . ' / ' . $this->date_to;
    }

    public static function getTypeArr()
    {
        return [
            'Простой',
            'Номерной'
        ];
    }

    public function getTypeText()
    {
        $items = Reliz::getTypeArr();
        return isset($items[$this->type]) ? $items[$this->type] : '';
    }

    public static function getMethodArr()
    {
        return [
            'Затарка',
            'Выдача на АМ'
        ];
    }

    public function getMethodText()
    {
        $items = Reliz::getMethodArr();
        return isset($items[$this->method]) ? $items[$this->method] : '';
    }

    public static function getStatusArr()
    {
        return ['Не исполнен','Активный'];
    }

    public function getStatusText()
    {
        $items = Reliz::getStatusArr();
        return isset($items[$this->status]) ? $items[$this->status] : '';
    }

    public function getLogText()
    {
        return '';
    }

    public function getReceptionDate()
    {
        return $this->date . '/' . $this->date_to;
    }

    public function getCountByReliz()
    {
        $c = Containers::find()
            ->select(['COUNT(*) AS cnt', 'reliz'])
            ->where( [ 'reliz'=> $this->number ] )
            ->one();
        return $c === null ? 0 : $c->cnt;
    }

    public function getRestCount()
    {
        return $this->amount - $this->getCountByReliz();
    }
}
