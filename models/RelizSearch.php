<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Reliz;

class RelizSearch extends Reliz
{
    public $date_s;
    public $date_e;

    public function rules()
    {
        return [
            [['id', 'amount', 'rest', 'method', 'type', 'status'], 'integer'],
            [['stock', 'type_cnt', 'date', 'date_to', 'number', 'buking', 'am', 'comment' , 'date_s', 'date_e'], 'safe'],
        ];
    }

    public function scenarios()
    {
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = Reliz::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'=> ['defaultOrder' => ['id'=>SORT_ASC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'amount' => $this->amount,
            'rest' => $this->rest,
            'date' => $this->date,
            'date_to' => $this->date_to,
            'method' => $this->method,
            'type' => $this->type,
            'status' => $this->status,
        ]);

        $query->andFilterWhere(['like', 'stock', $this->stock])
            ->andFilterWhere(['like', 'type_cnt', $this->type_cnt])
            ->andFilterWhere(['like', 'number', $this->number])
            ->andFilterWhere(['like', 'buking', $this->buking])
            ->andFilterWhere(['like', 'comment', $this->comment]);

        $query->andFilterWhere(['>=', 'date_to', $this->date_s]);
        $query->andFilterWhere(['<=', 'date_to', $this->date_e]);

        return $dataProvider;
    }
}
