<?php
namespace app\models;

use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;
/**
 * 
 */
class RoleForm extends Model
{
	
	public $name;
	public $description;
    public $permission;

	public function rules()
    {
        return [
            [['name',], 'required'],
            [['description', 'permission'], 'default'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'name' => 'имя',
            'description' => 'описание',
            'permission' => 'разрешение',
        ];
    }

    public static function getPermissions() {
        $auth = Yii::$app->authManager;
        $auth->getPermissions();
        $permissions = ArrayHelper::getColumn($auth->getPermissions(), 'description');
        return $permissions;
    }

    public static function getRoleByName($id) {
        $auth = Yii::$app->authManager;
        $name = $auth->getRole($id);
        return $name;
    }
}