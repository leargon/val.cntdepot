<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "stock".
 *
 * @property int $id
 * @property string $name
 * @property string $segment
 * @property int $uid
 */
class Stock extends \yii\db\ActiveRecord
{

    public static function tableName()
    {
        return 'stock';
    }

    public function rules()
    {
        return [
            [['name', 'segment', 'uid'], 'required'],
            [['uid'], 'integer'],
            [['name', 'segment'], 'string', 'max' => 255],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'id',
            'name' => 'Сток',
            'segment' => 'Сегмент',
            'uid' => 'Клиент',
            'user.org_r.name' => 'Организация',
        ];
    }

    public function getUser()
    {
        return $this->hasOne(Users::className(), ['id' => 'uid']);
    }

    public static function getStocks()
    {
        $records = Stock::find()->all();
        $items = ArrayHelper::map($records,'id','name');
        return ($items === NULL) ? [] : $items;
    }

    public static function getStockNames()
    {
        $records = Stock::find()->all();
        $items = ArrayHelper::map($records,'name','name');
        return ($items === NULL) ? [] : $items;
    }

    public static function getStockStatus($stock, $type_id, $type)
    {
        $sql = 'SELECT COUNT(*) as cnt FROM containers ';
        $sql .= 'WHERE operation = 0 AND conditions = 0 ';
        $sql .= 'AND type = "' . $type_id . '" AND stock = "' . $stock['id'] . '"';
        $container= Containers::findBySql($sql)->all();

        $sql = 'SELECT SUM(((reliz.amount - (SELECT COUNT(*) FROM containers c WHERE c.reliz=reliz.number)))) as sum FROM `reliz`';
        $sql .= 'WHERE `type_cnt` = "' . $type . '" AND `stock` = "' . $stock['name'] . '"';
        $sql .= 'AND ((reliz.`amount` - (SELECT COUNT(*) FROM containers c WHERE c.`reliz` = reliz.number)) > 0 ';
        $sql .= 'AND reliz.status != 3 AND (reliz.date_to >= CURRENT_DATE()))';

        $reliz = Reliz::findBySql($sql)->all();
        
        $cnt = count($container) ? $container[0]->cnt : 0;
        $sum = count($reliz) ? $reliz[0]->sum : 0;

        return $cnt - $sum;
    }
}
