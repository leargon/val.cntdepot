<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;
/**
 * This is the model class for table "types".
 *
 * @property int $id
 * @property string $name
 * @property string $iso
 * @property string $type
 */
class Types extends \yii\db\ActiveRecord
{

    public static function tableName()
    {
        return 'types';
    }

    public function rules()
    {
        return [
            [['name', 'iso', 'type'], 'required'],
            [['name', 'iso'], 'string', 'max' => 255],
            [['type'], 'string', 'max' => 100],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'id',
            'name' => 'Тип контейнера',
            'iso' => 'ISO',
            'type' => 'Тип',
        ];
    }

    public static function getTypes()
    {
        $records = Types::find()->all();
        $items = ArrayHelper::map($records,'id','name');
        return ($items === NULL) ? [] : $items;
    }

    public static function getTypeNames()
    {
        $records = Types::find()->all();
        $items = ArrayHelper::map($records,'name','name');
        return ($items === NULL) ? [] : $items;
    }
}
