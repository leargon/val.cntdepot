<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\web\UploadedFile;
use app\models\Containers;


class UploadForm extends Model
{
    /**
     * @var UploadedFile file attribute
     */
    public $import_in;
    public $import_out;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['import_in','import_out'], 'file'],
            [['import_in','import_out'], 'safe'],
            // [['import_in','import_out'], 'file', 'checkExtensionByMimeType' => false, 'extensions' => 'csv', 'mimeTypes' => 'text/plain'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'import_in' => 'Прием контейнеров',
            'import_out' => 'Выдача контейнеров',
        ];
    }

    private function formatDate($date) 
    {
    	$d = explode( "/",$date );
    	$time = strtotime($d[2] .'-'. $d[0] .'-'. $d[1]);
    	return date('Y-m-d',$time);
    }

    public function importIn($file)
    {
    	$path = dirname(__FILE__).'/../web/uploads/' . $file;
    	$rows = file($path);
    	foreach ($rows as $i => $row) 
    	{
    		if ($i > 0 && !empty($row) )
    		{
    			$cell = explode(";", $row);
    			if(empty($cell[0]))
    				continue;
    			$c = new Containers;
    			$c->user_id = Yii::$app->user->id;
    			$c->number = (string)$cell[0];
				$c->type = (string)$cell[1];
				// $c->date_in = $this->formatDate($cell[2]);
				$c->date_in = $cell[2];
				$c->am_number = (string)$cell[3];
				$c->conditions = $c->getCondId((string)$cell[4]);
				$c->status = trim($cell[5]) === "Порожний" ? 0 : 1;
				$c->stock = $c->getStockId((string)$cell[6]);
				$c->date_repair = (string)$cell[9];
			    $c->note = $cell[12];
				$c->block = trim($cell[13]) === "Нет" ? 0 : 1;
				if(!$c->save())
					var_dump($c->getErrors());
    		}
    	}
    	unlink($path);
    	return;
    }

    /**
     * Проверка
     */
    public function importOut($file)
    {
    	$path = dirname(__FILE__).'/../web/uploads/' . $file;
    	$rows = file($path);
    	foreach ($rows as $i => $row) 
    	{
    		if ($i > 0 && !empty($row) )
    		{
    			$cell = explode(";", $row);
    			if(empty($cell[0]))
    				continue;

    			$c = new Containers;
    			$c->user_id = Yii::$app->user->id;
    // 			'number' => (string)$cell[0],
				// 'container_type' => (string)$cell[1],
				// 'date_in' => str_replace("/","-",(string)$cell[2]),
				// 'am_number' => (string)$cell[3],
				// 'conditions' => getCond((string)$cell[4]),
				// 'status' => trim($cell[5]) === "Порожний" ? 0 : 1,
				// 'stock' => (string) getStock((string)$cell[6]),
				// 'date_out' => (string)$cell[7],
				// 'am2' => (string)$cell[8],
				// 'date_repair' => (string)$cell[9],
				// 'buking' => $cell[10],
				// 'id_reliz' => $cell[11],
    //             'note' => (float)$cell[12],
				// 'block' => trim($cell[13]) === "Нет" ? 0 : 1,
    			$c->number = $cell[0];
				$c->type = $cell[1];
				$c->date_in = $cell[2];
				$c->am_number = $cell[3];
				$c->conditions = $c->getCondId($cell[4]);
				$c->status = trim($cell[5]) === "Порожний" ? 0 : 1;
				$c->stock = $c->getStockId($cell[6]);
				// $c->date_out => str_replace('.', '-', $cell[7]);
				$c->date_out = $cell[7];
				$c->am2 = $cell[8];
				$c->date_repair = $cell[9];
				$c->buking = $cell[10];
				$c->id_reliz = $cell[11];
			    $c->note = $cell[12];
				$c->block = trim($cell[13]) === "Нет" ? 0 : 1;
				if(!$c->save()) {
					var_dump($c->getErrors());exit;
				}
    		}
    	}
    	unlink($path);
    	return;
    }
}