<?php

namespace app\models;

use \Yii;
use yii\helpers\ArrayHelper;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

/**
 * This is the model class for table "users".
 *
 * @property int $id
 * @property string $login
 * @property string $password
 * @property string $new_password
 * @property int $role
 * @property int $organization
 * @property string $email
 * @property int $edi
 */
class Users extends \yii\db\ActiveRecord implements IdentityInterface
{

    protected $authKey;
    public $new_password;

    const ROLE_CLIENT = 'Клиент';
    const ROLE_REMZONA = 'Ремзона';
    const ROLE_DISPATCHER = 'Диспечер';
    const ROLE_ADMIN = 'Администратор';

    const SCENARIO_LOGIN = 'login';
    const SCENARIO_CREATE = 'create';
    const SCENARIO_UPDATE = 'update';

    public static function tableName()
    {
        return 'users';
    }

    public function rules()
    {
        return [
            [['login', 'email', 'new_password', 'organization', 'edi'], 'required', 'on' => self::SCENARIO_CREATE],
            [['login'], 'required', 'on' => self::SCENARIO_UPDATE],
            ['login', 'unique'],
            [['role'], 'integer'],
            [['login'], 'string', 'max' => 30],
            [['password'], 'string', 'max' => 32],
            [['organization', 'email'], 'string', 'max' => 255],
            [['organization', 'email', 'edi', 'new_password'], 'safe'],
        ];
    }

    public function scenarios()
    {
        return [
            self::SCENARIO_CREATE => ['login', 'new_password', 'email', 'organization', 'role', 'edi'],
            self::SCENARIO_UPDATE => ['new_password', 'email','edi', 'organization','role'],
        ];
    }

    /**
     * Атрибуты
     */
    public function attributeLabels()
    {
        return [
            'id' => 'id',
            'login' => 'Логин',
            'password' => 'Пароль',
            'new_password' => 'Пароль',
            'role' => 'Роль',
            'organization' => 'Организация',
            'org' => 'Организация',
            'email' => 'e-mail',
            'edi' => 'EDI-статус',
            'roleText' => 'Роль',
            'ediText' => 'EDI-статус',
        ];
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) 
        {
            if(!empty($this->new_password))
            {
                $this->setPassword($this->new_password);
            }
            return true;
        } else {
            return false;
        }
    }

    /**
     * Список организации из таблицы Orgs
     */
    public function getOrg_r()
    {
        return $this->hasOne(Orgs::className(), ['id' => 'organization']);
    }
    
    /**
     * Роли пользователя
     */
    public static function getRoleArr()
    {
        return [
            Users::ROLE_CLIENT,
            Users::ROLE_REMZONA,
            Users::ROLE_DISPATCHER,
            Users::ROLE_ADMIN
        ];
    }

    public function getRoleText()
    {
        $items = Users::getRoleArr();
        return isset($items[$this->role]) ? $items[$this->role] : '';
    }
    
    public static function getEdiArr()
    {
        return ['Приостановленно', 'Отправлять'];
    }

    public function getEdiText()
    {
        $items = Users::getEdiArr();
        return isset($items[$this->edi]) ? $items[$this->edi] : '';
    }


    public static function getUsers()
    {
        $records = Users::find()->all();
        $items = ArrayHelper::map($records,'id','login');
        return ($items === NULL) ? [] : $items;
    }

    /**
     * Авторизация
     */
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id]);
    }
 
    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }
 
    /**
     * Поиск пользователя по имени
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($login)
    {
        return static::findOne(['login' => $login]);
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }
    
    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->authKey;
    }
 
    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Проверка пароля
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return $this->password === md5(md5($password));
    }
 
    /**
     * Установить пароль
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password = md5(md5($password));
    }
 
    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

}
