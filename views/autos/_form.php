<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>

<div class="am-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'number')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'driver')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'reliz_id')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
