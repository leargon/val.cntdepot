<?php

use yii\helpers\Html;

$this->title = 'Добавить';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="am-create">

    <h3><?= Html::encode($this->title) ?></h3>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
