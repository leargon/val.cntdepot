<?php

use yii\helpers\Html;
use yii\grid\GridView;

$this->title = 'Автомобили';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="am-index">

    <p>
        <?= Html::a('Фильтр', ['#'], ['class' => 'btn btn-success','onclick' => '$( ".filters" ).toggle();return false;']) ?>
        <?= Html::a('Добавить', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'summary' => false,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'number:ntext',
            'driver:ntext',
            'reliz_id',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
