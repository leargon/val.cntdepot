<?php

use yii\helpers\Html;

$this->title = 'Обновить ' . $model->number;
$this->params['breadcrumbs'][] = 'Обновить';
?>
<div class="am-update">

    <h3><?= Html::encode($this->title) ?></h3>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
