<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use kartik\date\DatePicker;

$this->registerCssFile("/css/check_table.css");

?>

<table class="custom-list table table-striped container-search">
    <tr>
        <td></td>
        <td>Дата отгрузки</td>
        <td>Номер</td>
        <td>Тип</td>
        <td>Дата прихода</td>
        <td>Номер АМ</td>
        <td>Состояние</td>
        <td></td>
        <td>Дата ремонта</td>
        <td>Примечание</td>
    </tr>

    <?php 
    foreach($containers as $c): 
        $button_text = $c->type_r->type !== 'REEF' ? 'Отгрузить' : 'Отгрузить с генератором';
    ?>

    <tr class="t_r">
        <td>
            <button 
                class="btn btn-primary shipping" 
                container-id="<?= $c->id ?>" 
                reliz-id="<?= $reliz !== null ? $reliz->id : 0; ?>" 
                auto-id="<?= $auto !== null ? $auto->id : 0 ?>"><?= $button_text ?>
            </button>
        </td>
        <td>
            <input value="<?=date('Y-m-d', time())?>" class="d_<?=$c->id ?> short-input"/>
        </td>
        <td><?= $c->number; ?></td>
        <td><?= $c->type_r->name; ?></td>
        <td><?= $c->date_in; ?></td>
        <td><?= $c->am_number; ?></td>
        <td><?= $c->statusText; ?></td>
        <td><?= $c->condText; ?></td>
        <td><?= $c->date_repair; ?></td>
        <td><?= $c->note; ?></td>
    </tr>

    <?php endforeach; ?>

</table>

<script type="text/javascript">

    $('.shipping').click(function() {

        var container_id    = $(this).attr('container-id');
        var date_out        = $('.d_' + container_id).val();
        var reliz_id        = $(this).attr('reliz-id');
        var auto_id         = $(this).attr('auto-id');

        var params = "";
        params = "&container_id=" + container_id
        params += "&reliz_id=" + reliz_id 
        params += "&auto_id=" + auto_id 
        params += "&date_out=" + date_out;

        $.ajax({
            url: "<?= Url::toRoute(['check/save-reliz-container'])?>" + params,
        }).done(function(data) {
            window.location = "<?= Url::toRoute(['reliz/reliz'])?>" + "&id=" +  reliz_id;
        });

       })

</script>