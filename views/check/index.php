<?php

use yii\helpers\Url;

$this->registerCssFile("/css/check_table.css");
?>

<table class="table table-struct">
		<tbody><tr>
			<td> 	
				<form method="POST" id="chkForm">
				<table class="num-table">
					<tbody><tr> 
						<td class="td-btn" colspan="3">
						
								<input name="check" id="num" type="text" class="form-control" value="">
							
						</td>
					</tr>
					<tr>
						<td class="td-btn"><div class="num-btn">7</div></td>
						<td class="td-btn"><div class="num-btn">8</div></td>
						<td class="td-btn"><div class="num-btn">9</div></td>
					</tr>
					<tr>
						<td class="td-btn"><div class="num-btn">4</div></td>
						<td class="td-btn"><div class="num-btn">5</div></td>
						<td class="td-btn"><div class="num-btn">6</div></td>
					</tr>
					<tr>
						<td class="td-btn"><div class="num-btn">1</div></td>
						<td class="td-btn"><div class="num-btn">2</div></td>
						<td class="td-btn"><div class="num-btn">3</div></td>
					</tr>
					<tr>
						<td class="td-btn"><div class="num-btn">0</div></td>
						<td class="td-btn"><div class="num-btn">Del</div></td>
						<td class="td-btn"><div class="num-btn">X</div></td>
					</tr>
					<tr> 
						<td сlass="td-btn" colspan="3">
							<div class="num-btn" id="sbmt"><input type="submit" value="Искать"></div>
						</td>
					</tr>
				</tbody></table>
				</form>
			</td>
			<td id="content"></td>
		</tr>
	</tbody>
</table>
<script>
	window.onload = function() {
		$('.td-btn').click(function(o){ 
			var f = jQuery('#num').val();
			var i = jQuery(this).find('div:first').text();
			if ( i === 'Del') { 
				jQuery('#num').val(f.substring(0, f.length - 1));	
			} else if ( i === 'X' ) { 
				jQuery('#num').val('');
			}else {
				jQuery('#num').val(f + i);
			}
		});
		$('#sbmt').click(function(o){ 
			$.post(
				'<?= Url::toRoute("/check/ajax");?>', 
				{'id': $('#num').val()},
					function(data) { 
						$('#content').html(data);
						return data; 
				});
			return false;
		});
					}
</script>