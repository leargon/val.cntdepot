<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;

$this->registerCssFile("/css/check_table.css");

?>

<table class="custom-list table table-striped">
    <?php foreach($containers as $c): ?>
        <?php
            $num = str_replace($pattern, '<span class="green">' . $pattern . '</span>', $c->number);
            if ($c->block === 0)
            {
                $img = '<span class="shipping-btn menu-icon"></span>';
                $cond_btn =  Html::a(
                    $img, 
                    ['containers/shipping','id' => $c->id ]
                );
            } else {
                $img = '<span class="block-btn menu-icon"></span>';
                $cond_btn = Html::a(
                    $img, 
                    ['containers/block','id' => $c->id ],
                    [
                        'data' => [
                            'confirm' => 'Снять блок?',
                            'method' => 'post',
                        ]
                    ]
                );
            }
    ?>
    <tr>
        <td>Номер</td>
        <td><b><?= $num; ?></b></td>
        <td></td>
    </tr>
    <tr>
        <td>Тип</td>
        <td><?= $c->type; ?></td>
        <td>
            <?= $cond_btn; ?>
            <!-- <a href="<?= Url::toRoute(['/containers/shipping','id' => $c->id]); ?>">
            <span class="shipping-btn menu-icon"></span>
            </a> -->
        </td>
    </tr>
    <tr>
        <td>Сток</td>
        <td><?= $c->stock_r !== null ? $c->stock_r->name : ''; ?></td>
        <td>
            <a href="<?= Url::toRoute(['/containers/update','id' => $c->id]); ?>">
               <span class="update-btn menu-icon"></span>
            </a>
        </td>
    </tr>
    <tr>
        <td>Статус</td>
        <td><?= $c->statusText; ?></td>
        <td>
            <?= Html::a(
                    '<span class="trash-btn menu-icon"></span>', 
                    ['delete','id' => $c->id ],
                    [
                        'data' => [
                            'confirm' => 'Удалить запись?',
                            'method' => 'post',
                        ]
                    ])
            ?>
        </td>
    </tr>
    <tr>
        <td>Блок</td>
        <td><span class="<?= !$c->block ? 'green' : 'red'?>"><?= $c->blockText; ?></span></td>
        <td>
            <a href="<?= Url::toRoute(['/containers/contracts','id' => $c->id]); ?>">
                <span class="config-btn menu-icon"></span>
            </a>
        </td>
    </tr>
    <tr>
        <td>Состояние</td>
        <td><span class="<?= !$c->block ? 'green' : 'red'?>"><?= $c->condText; ?></span></td>
        <td></td>
    </tr>
    <tr>
        <td>Комментарии</td>
        <td><?= empty($c->note) ? "" : $c->note; ?></td>
        <td></td>
    </tr>
    <tr>
        <td style="padding-left: 0px !important;" class="td-hr" colspan="3"><hr></td>
    </tr>
    <?php endforeach; ?>
</table>