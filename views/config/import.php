<?php

use yii\widgets\ActiveForm;

?>

<div>

	<?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]) ?>

	<p><?= $form->field($model, 'import_in')->fileInput() ?></p>

	<p><?= $form->field($model, 'import_out')->fileInput() ?></p>

	<p><button>Отправить</button></p>

	<?php ActiveForm::end() ?>

</div>