<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Stock;
use app\models\Types;
use app\models\Containers;

use kartik\date\DatePicker;

$model->date_in = date("Y-m-d",time());
$model->time_in = time();

?>

<div class="containers-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'stock')->dropDownList(Stock::getStocks()) ?>

    <?= $form->field($model, 'number')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'type')->dropDownList(Types::getTypes()) ?>

    <?= $form->field($model, 'date_in')->widget(DatePicker::classname(), [
        'options' => ['placeholder' => ''],
        'pluginOptions' => [
            'autoclose'=>true,
            'format' => 'yyyy-mm-dd'
        ]
    ]) ?>

    <?= $form->field($model, 'am_number')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'conditions')->dropDownList(Containers::getCondArr()) ?>

    <?= $form->field($model, 'status')->dropDownList(Containers::getStatusArr()) ?>

    <?= $form->field($model, 'block')->dropDownList(Containers::getBlockArr()) ?>

    <?= $form->field($model, 'note')->textarea(['rows' => 6]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Добавить' : 'Изменить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<script>
    function getZnak(con) {
        con = con.toLowerCase();
        var words = con.split('');
        var length = words.length;
        
        var values = {
            'a': 10,        'n': 25,
            'b': 12,        'o': 26,
            'c': 13,        'p': 27,
            'd': 14,        'q': 28,
            'e': 15,        'r': 29,
            'f': 16,        's': 30,
            'g': 17,        't': 31,
            'h': 18,        'u': 32,
            'i': 19,        'v': 34,
            'j': 20,        'w': 35,
            'k': 21,        'x': 36,
            'l': 23,        'y': 37,
            'm': 24,        'z': 38,
            '0': 0
        };
    
        
        var sum = 0;
        for(var i = 0; i < length; i++) {
            var v = parseInt(words[i]) ? words[i] : values[words[i]];
            var p = Math.pow(2, i);
            var m = p * parseInt(v);
            
            sum += m;
        }
            
        var ost = sum % 11;
    
        return ost;
    }

    $('#w0').on('beforeSubmit', function (e) {
        var input = $("#containers-number");     
        var value = input.val();
        value = value.trim();
        var znak = getZnak(value.substring(0, value.length-1));         
        var last = parseInt(value[value.length - 1]);
        if (znak != last && !confirm("Неверный номер. Нажмите ОК для добавления, ОТМЕНА для правки")) {
            return false;
        }
        return true;
    });
</script>
