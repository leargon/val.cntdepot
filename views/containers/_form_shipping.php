<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Stock;
use app\models\Types;
use app\models\Containers;

use kartik\date\DatePicker;

// $model->date_out = date("d.m.Y",time());

?>

<div class="containers-info">
    
    <h2>Форма отгрузки контейнера</h2>
    <table class="table table-striped table-bordered">
        <tr>
            <td>Номер контейнера</td>
            <td>Тип контейнера</td>
            <td>Дата прихода</td>
            <td>Номер АМ</td>
            <td>Состояние контейнера</td>
            <td>Порожний/груженный</td>
            <td>Сток</td>
            <td>Дата ремонта</td>
        </tr>
        <tr>
            <td><?= $model->number ?></td>
            <td><?= $model->type_r->name ?></td>
            <td><?= $model->date_in ?></td>
            <td><?= $model->am_number ?></td>
            <td><?= $model->condText ?></td>
            <td><?= $model->statusText ?></td>
            <td><?= $model->stock_r->name ?></td>
            <td><?= $model->date_repair ?></td>
        </tr>
    </table>

</div>

<div class="containers-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'reliz')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'buking')->textInput(['maxlength' => true]) ?>


    <?= $form->field($model, 'date_out')->widget(DatePicker::classname(), [
        'options' => ['placeholder' => ''],
        'pluginOptions' => [
            'autoclose'=>true,
            'format' => 'yyyy-mm-dd'
        ]
    ]) ?>

    <?= $form->field($model, 'am_number')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'note')->textarea(['rows' => 6]) ?>

    <div class="form-group">
        <?= Html::submitButton('Отгрузить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
