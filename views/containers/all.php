<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\models\Users;
use app\models\Stock;
use app\models\Types;
use app\models\Containers;
use yii\helpers\ArrayHelper;
use kartik\date\DatePicker;

$this->title = 'Отчеты по контейнерам';
$this->params['breadcrumbs'][] = ['label' => 'Контейнеры', 'url' => ['index']];
$this->params['breadcrumbs'][] =  'Отчеты по контейнерам';

$role = Yii::$app->user->identity->roleText;

$buttons['class'] = 'yii\grid\ActionColumn';
$buttons['template'] =  Users::ROLE_ADMIN === $role ? 
    '{shipping} {update} {delete} {contracts}' : '{view} {update} {delete}';

$buttons['buttons'] = [
    'shipping' => function ($url, $model) 
    {
        if ($model->block)
        {
            return Html::a('<span class="block-btn menu-icon"></span>', 
                ['block','id' => $model->id ],
                [
                    'data' => [
                        'confirm' => 'Снять блок?',
                        'method' => 'post',
                    ]
                ]);
        } else {
            return Html::a('<span class="shipping-btn menu-icon"></span>', 
                ['shipping','id' => $model->id ]);
        }
    },
    'update' => function ($url, $model) 
    {
        return Html::a('<span class="update-btn menu-icon"></span>', 
            ['update','id' => $model->id ]);
    },
    'delete' => function ($url, $model) 
    {
        return Html::a('<span class="trash-btn menu-icon"></span>', 
                ['delete','id' => $model->id ],
                [
                    'data' => [
                        'confirm' => 'Удалить запись?',
                        'method' => 'post',
                    ]
                ]);
    },
    'contracts' => function ($url, $model) 
    {
        return Html::a('<span class="config-btn menu-icon"></span>', ['contracts','id' => $model->id ]);
    }
];


?>
<div class="containers-index">

    <h3><?= Html::encode($this->title) ?></h3>

    <p id="btn-block">
        <?= Html::a('Фильтр', ['#'], ['class' => 'btn btn-success','onclick' => '$( ".filters" ).toggle();;return false;']) ?>
        <?= Html::a('Сбросить фильтр', ['all'], ['class' => 'btn btn-success']) ?>
        <?= Html::a('Контейнеры без отгрузки', ['index'], ['class' => 'btn btn-success']) ?>
        <?= Html::a('Список загруженных контейнеров', ['index'], ['class' => 'btn btn-success']) ?>
        <?= Html::a('Экспортировать в XLS', ['exportall'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'summary' => false,
        'tableOptions' => [
            'class' => 'table table-striped table-bordered data-table'
        ],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'number',
            [
                'attribute' => 'type_r.name',
                'filter' => ArrayHelper::map(Types::find()->all(), 'id', 'name'),
                'filterInputOptions' => ['class' => 'form-control form-control-sm', 'style' => 'width: 80px;'],
            ],
            [
                'attribute' => 'date_in',
                'format' => 'datetime',
                'headerOptions' => [
                    'class' => 'col-md-2'
                ],
                'filter' => DatePicker::widget([
                    'model' => $searchModel,
                    'attribute' => 'date_in_s',
                    'attribute2' => 'date_in_e',
                    'options' => ['placeholder' => 'От'],
                    'options2' => ['placeholder' => 'До'],
                    'type' => DatePicker::TYPE_RANGE,
                    'separator' => '-',
                    'pluginOptions' => ['format' => 'yyyy-mm-dd'],
                ]),
                'format' => ['date', 'Y-MM-dd'],
                'filterInputOptions' => ['class' => 'form-control form-control-sm', 'style' => 'width: 80px;'],
            ],
            'am_number',
            [
                'attribute' => 'conditions',
                'value' => 'condText',
                'filter' => Containers::getCondArr(),
                'filterInputOptions' => ['class' => 'form-control form-control-sm', 'style' => 'width: 114px;'],
            ],
            [
                'attribute' => 'status',
                'value' => 'statusText',
                'filter' => Containers::getStatusArr(),
                'filterInputOptions' => ['class' => 'form-control form-control-sm', 'style' => 'width: 108px;'],
            ],
            [
                'attribute' => 'stock',
                'value' => 'stock_r.name',
                'filter' => ArrayHelper::map(Stock::find()->all(), 'id', 'name'),
                'filterInputOptions' => ['class' => 'form-control form-control-sm', 'style' => 'width: 135px;'],
            ],
            [
                'attribute' => 'date_out',
                'format' => 'datetime',
                'headerOptions' => [
                    'class' => 'col-md-2'
                ],
                'filter' => DatePicker::widget([
                    'model' => $searchModel,
                    'attribute' => 'date_out_s',
                    'attribute2' => 'date_out_e',
                    'options' => ['placeholder' => 'От'],
                    'options2' => ['placeholder' => 'До'],
                    'type' => DatePicker::TYPE_RANGE,
                    'separator' => '-',
                    'pluginOptions' => ['format' => 'yyyy-mm-dd'],
                ]),
                'value' =>    function ($model) {
                    return $model->date_out !== '0000-00-00' ? $model->date_out : null;
                },
                'format' => ['date', 'Y-MM-dd'],
                'filterInputOptions' => ['class' => 'form-control form-control-sm date-filter'],

            ],
            'buking',
            'reliz',
            [
                'attribute' => 'date_repair',
                'format' => 'datetime',
                'headerOptions' => [
                    'class' => 'col-md-2'
                ],
                'filter' => DatePicker::widget([
                    'model' => $searchModel,
                    'attribute' => 'date_repair_s',
                    'attribute2' => 'date_repair_e',
                    'options' => ['placeholder' => 'От'],
                    'options2' => ['placeholder' => 'До'],
                    'type' => DatePicker::TYPE_RANGE,
                    'separator' => '-',
                    'pluginOptions' => ['format' => 'yyyy-mm-dd'],
                ]),
                'value' =>    function ($model) {
                    return $model->date_repair !== '0000-00-00' ? $model->date_repair : null;
                },
                'format' => ['date', 'Y-MM-dd'],
                'filterInputOptions' => ['class' => 'form-control form-control-sm date-filter'],

            ],
            'note:ntext',
            [
                'attribute' => 'block',
                'value' => 'blockText',
                'filter' => Containers::getBlockArr(),
                'filterInputOptions' => ['class' => 'form-control form-control-sm', 'style' => 'width: 80px;'],
            ],
            $buttons,
        ],
    ]); ?>
</div>
