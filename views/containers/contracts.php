<?php

use app\models\Contracts;
use app\models\Hoursprice;
use yii\helpers\Url;


$this->registerJsFile('/js/contracts.js',  ['position' => yii\web\View::POS_END]);
?>
<div class="containers-info">
    
    <table class="table-striped table-bordered">
    	<tr>
    		<td>Номер контейнера</td>
            <td><?= $model->number ?></td>
    	</tr>
    	<tr>
            <td>Тип контейнера</td>
            <td><?= $model->type_r->type ?></td>
    	</tr>
    	<tr>
            <td>Размер</td>
            <td><?= $model->type_r->name ?></td>
    	</tr>
    	<tr>
            <td>Сток</td>
            <td><?= $model->stock_r->name ?></td>
    	</tr>
    	<tr>
            <td>Дата приема</td>
            <td><?= $model->date_in ?></td>
    	</tr>
    	<tr>
    		<td colspan="2"><?= $model->note ?></td>
    	</tr>
    </table>

</div>
<div id="contract-info" style="margin-bottom: 20px;">
	<h2>Текущая смета</h2>
	<?= stripslashes(htmlspecialchars_decode($calc->html)); ?>
</div>
<div id="contracts">
	<h2>Новая смета</h2>
	<table class="contract-table table table-striped table-bordered">
		<tr class="thead">
			<td>Номер, пп</td>
			<td>Кол-во, шт</td>
			<td>Описание</td>
			<td>Код ремонта</td>
			<td>Loc Code</td>
			<td>Материал за шт.</td>
			<td>Нормочасов за шт</td> 
			<td>Итого по виду работ</td>
		</tr>
	</table>
</div>

<div class="containers-index" style="font-size: 16px;">

	<?php if ($this->beginCache('Contracts'.$model->stock_r->name . '-' . $model->type_r->type)) : ?>

	<table class="table table-striped table-bordered table-contracts">
		<tr>
			<td>
				<ul id="contract-tree" class="contracts-list">
					
				<?php foreach (Contracts::getLocations($model->stock_r->user->organization, $model->type_r->name) as $k => $l) : ?>

					<?php if(empty($l->location)) continue; ?>
					
					<li class="location"><?= $l->location ?></li>
					<ul class="contracts-list" style="display: none">

						<?php foreach (Contracts::getElements($l->location, $model->type_r->name) as $k => $e) : ?>

							<li class="element"><?= $e->element ?></li>

							<ul class="contracts-list" style="display: none">

								<?php foreach (Contracts::getTypes($e->element, $model->type_r->name,$l->location) as $k => $t) : ?>
									<li class="repair-type" data-id="<?= $t->id ?>">
										<?= $t->repair_type ?>
									</li>
								<?php endforeach; ?>

							</ul>

						<?php endforeach; ?>

					</ul>

				<?php endforeach; ?>

				</ul>
			</td>
			<td id="contact-select">
				<span class="contract-add">></span><br/>
				<span class="contract-del"><</span>
			</td>
			<td>
				<h5>Выбранный вид работ</h5>
				<ul id="types-selected"></ul>
			</td>
			<td>
				<h5>Параметры</h5>
				<div id="contract-params"></div>
			</td>
		</tr>
	</table>

	<?php $this->endCache();  endif; ?>
	
	<script type="text/javascript">

		var current_date = "<?= date('Y-m-d', time()) ?>";
		var container_id = <?= $model->id ?>;
		var container_type = '<?= $model->type_r->name ?>';
		var url_index = "<?= Url::toRoute(['index'])?>";
		var url_save = "<?= Url::toRoute(['save-contracts'])?>";
		var url_get_contracts = "<?= Url::toRoute(['contracts-ajax', 'id' => ''])?>";
		var url_get_contracts_repair = "<?= Url::toRoute(['contracts-ajax-repair', 'hours' => ''])?>";
		var hours_price = <?= Hoursprice::getPrice($model->type_r->name);?>;

	</script>

</div>
