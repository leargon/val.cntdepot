<style>
/*.grid-view th {
    white-space: normal;
}*/
.grid-view th {
    text-align: center;
}
@media (min-width: 1200px) {
  .container {
    width: 1280px;
  }
}
.input-daterange {
    width: 100% !important;
}
.filter-custom {
    font-size: 12px;
    padding: 0;
    height: auto;
    width: 100%;
}
</style>
<?php

use app\models\Calc;
use yii\helpers\Html;
use yii\grid\GridView;
use app\models\Users;
use app\models\Stock;
use app\models\Types;
use app\models\Containers;
use yii\helpers\ArrayHelper;
use kartik\date\DatePicker;
use yii\helpers\VarDumper;
use mdm\admin\components\Helper;

$this->title = 'Контейнеры';
$this->params['breadcrumbs'][] = $this->title;

$role = Yii::$app->user->identity->roleText;

$buttons['class'] = 'yii\grid\ActionColumn';

$buttons['header'] = 'Операции';
$buttons['template'] =  Helper::filterActionColumn('{shipping}{update}{delete}{contracts}');
// $buttons['template'] =  Users::ROLE_ADMIN === $role ? 
//     '{shipping} {update} {delete} {contracts}' : '{update} {delete}';

$buttons['buttons'] = [
    'shipping' => function ($url, $model) 
    {
        if ($model->block && $model->status != 2)
        {
            return Html::a('<span class="block-btn menu-icon"></span>', 
                ['block','id' => $model->id ],
                [
                    'data' => [
                        'confirm' => 'Снять блок?',
                        'method' => 'post',
                    ]
                ]);
        } elseif($model->status != 2) {
            return Html::a('<span class="shipping-btn menu-icon"></span>', 
                ['shipping','id' => $model->id ]);
        }
    },
    'update' => function ($url, $model) 
    {   
        if ($model->status != 2) {
            return Html::a('<span class="update-btn menu-icon"></span>', 
            ['update','id' => $model->id ]);
        }
        
    },
    'delete' => function ($url, $model) 
    {
        if ($model->status != 2) {
            return Html::a('<span class="trash-btn menu-icon"></span>', 
                ['delete','id' => $model->id ],
                [
                    'data' => [
                        'confirm' => 'Удалить запись?',
                        'method' => 'post',
                    ]
                ]);
        }
        
    },
    'contracts' => function ($url, $model) 
    {   
        if ($model->status != 2) {
            return Html::a('<span class="config-btn menu-icon"></span>', ['contracts','id' => $model->id ]);
        }
        
    }
];

?>
<div class="containers-index">

    <p id="btn-block">
        <?= Html::a('Фильтр', ['#'], ['class' => 'btn btn-success','onclick' => '$( ".filters" ).toggle();$(".search-submit-circle").toggle();return false;']) ?>
        <?= Html::a('Сбросить фильтр', ['index'], ['class' => 'btn btn-success']) ?>
        <?= Html::a('Отчеты', ['all'], ['class' => 'btn btn-success']) ?>
        <?= Html::a('Список загруженных контейнеров', ['list'], ['class' => 'btn btn-success']) ?>
        <?= Html::a('Экспортировать в XLS', ['export'], ['class' => 'btn btn-success', 'data-method' => 'post', 'data-params' => [ 'ids' =>$ids]]) ?>
        <?= Html::submitButton('Применить фильтр', ['class' => 'btn btn-warning search-submit-circle', 'style' => 'display:none;']) ?>
    </p>
    <?php 
        $page = isset($_GET['page']) ? $_GET['page'] : 1;
        $sort = isset($_GET['sort']) ? $_GET['sort'] : 1;
        $cache_key = 'ContainersIndex_' . $role . '_' . $page . '_' . $sort;
        if ($this->beginCache($cache_key)) : 
    ?>
    <?php 
        //VarDumper::dump($dataProvider);
    ?>
    <div class="table-responsive">
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'rowOptions' => function($dataProvider){
            if ($dataProvider->status == 2) {
                return ['class' => 'bg-secondary'];
            }

            // Проверка если смета с кодом ошибки то окрашиваем ее
            // if ($dataProvider->calc['error_code'] == 1) {
            //     return ['class' => 'warning'];
            // }
            // if ($dataProvider->calc['error_code'] == 0) {
            //     return ['class' => 'success'];
            // }
            // if ($dataProvider->calc['error_code'] == 2) {
            //     return ['class' => 'danger'];
            // }
        },
        'headerRowOptions' => [
            'class' => 'bg-info'
        ],
        'tableOptions' => [
            'class' => 'table table-striped table-bordered data-table table-condensed table-responsive',
            'style' => 'font-size: 12px;text-align:center;'
        ],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'number',
                'filterInputOptions' => ['class' => 'form-control form-control-sm filter-custom'],
                //'headerOptions'=>['style'=>'width: 50px;'],
                //'contentOptions'=>['style'=>'width: 50px; max-width: 50px;word-wrap:break-word;']
            ],
            [
                'label' => 'Тип ко-ра',
                'value' => 'type_r.name',
                'attribute' => 'type',
                'filter' => ArrayHelper::map(Types::find()->all(), 'id', 'name'),
                'filterInputOptions' => ['class' => 'form-control form-control-sm filter-custom'],
                'headerOptions'=>['style'=>'width: 50px; max-width: 50px;word-wrap:break-word;white-space: normal;'],
                'contentOptions'=>['style'=>'width: 50px; max-width: 50px;word-wrap:break-word;']
            ],
            [
                'attribute' => 'date_in',
                'format' => 'datetime',
                'headerOptions' => [
                    'class' => 'col-md-1',
                    'style'=>'white-space: normal;'
                ],
                'filter' => DatePicker::widget([
                    'model' => $searchModel,
                    'attribute' => 'date_in_s',
                    'attribute2' => 'date_in_e',
                    'options' => ['placeholder' => 'От', 'class' => 'filter-custom'],
                    'options2' => ['placeholder' => 'До', 'class' => 'filter-custom'],
                    'type' => DatePicker::TYPE_RANGE,
                    'separator' => '-',
                    'pluginOptions' => ['format' => 'yyyy-mm-dd'],
                ]),
                'format' => ['date', 'Y-MM-dd'],
                'contentOptions'=>['style'=>'white-space: nowrap;']
            ],
            //'am_number',
            [
                'label' => 'Номер АМ',
                'attribute' => 'am_number',
                'visible' => Yii::$app->user->can('viewColumn'),
                'headerOptions'=>['style'=>'white-space: normal;'],
                'filterInputOptions' => ['class' => 'filter-custom form-control form-control-sm'],
                'contentOptions'=>['style'=>'white-space: nowrap;']
            ],
            [
                'attribute' => 'conditions',
                'value' => 'condText',
                'filter' => Containers::getCondArr(),
                'filterInputOptions' => ['class' => 'form-control form-control-sm filter-custom'],
                'headerOptions'=>['style'=>'white-space: normal;'],
                'contentOptions'=>['style'=>'white-space: nowrap;']
            ],
            [
                'attribute' => 'status',
                'value' => 'statusText',
                'filter' => Containers::getStatusArr(),
                'filterInputOptions' => ['class' => 'form-control form-control-sm filter-custom'],
                'headerOptions'=>['style'=>'white-space: normal;'],
                'contentOptions'=>['style'=>'white-space: nowrap;']
            ],
            [
                'attribute' => 'stock',
                'value' => 'stock_r.name',
                'filter' => ArrayHelper::map(Stock::find()->all(), 'id', 'name'),
                'filterInputOptions' => ['class' => 'form-control form-control-sm filter-custom'],
                'headerOptions'=>['style'=>'width: 50px; max-width: 50px;word-wrap:break-word;white-space: normal;'],
                'contentOptions'=>['style'=>'white-space: nowrap;']
            ],
            [
                'attribute' => 'date_out',
                'format' => 'datetime',
                'headerOptions' => [
                    'class' => 'col-md-1',
                    'style'=>'white-space: normal;'
                ],
                'filter' => DatePicker::widget([
                    'model' => $searchModel,
                    'attribute' => 'date_out_s',
                    'attribute2' => 'date_out_e',
                    'options' => ['placeholder' => 'От', 'class' => 'filter-custom'],
                    'options2' => ['placeholder' => 'До', 'class' => 'filter-custom'],
                    'type' => DatePicker::TYPE_RANGE,
                    'separator' => '-',
                    'pluginOptions' => ['format' => 'yyyy-mm-dd'],
                ]),
                'value' => function ($model) {
                    return $model->date_out !== '0000-00-00' ? $model->date_out : null;
                },
                'format' => ['date', 'Y-MM-dd'],
                'filterInputOptions' => ['class' => 'form-control form-control-sm date-filter'],
                'contentOptions'=>['style'=>'white-space: nowrap;']

            ],
            //'am2',
            [
                'label' => 'АМ Комп.',
                'attribute' => 'am2',
                'visible' => Yii::$app->user->can('viewColumn'),
                'headerOptions'=>['style'=>'white-space: normal;'],
                'filterInputOptions' => ['class' => 'filter-custom form-control form-control-sm'],
                'contentOptions'=>['style'=>'white-space: nowrap;']
            ],
            [
                'attribute' => 'date_repair',
                'format' => 'datetime',
                'headerOptions' => [
                    'class' => 'col-md-1',
                    'style'=>'white-space: normal;'
                ],
                'filter' => DatePicker::widget([
                    'model' => $searchModel,
                    'attribute' => 'date_repair_s',
                    'attribute2' => 'date_repair_e',
                    'options' => ['placeholder' => 'От', 'class' => 'filter-custom'],
                    'options2' => ['placeholder' => 'До', 'class' => 'filter-custom'],
                    'type' => DatePicker::TYPE_RANGE,
                    'separator' => '-',
                    'pluginOptions' => ['format' => 'yyyy-mm-dd'],
                ]),
                'value' => function ($model) {
                    return $model->date_repair !== '0000-00-00' ? $model->date_repair : null;
                },
                'format' => ['date', 'Y-MM-dd'],
                'filterInputOptions' => ['class' => 'form-control form-control-sm date-filter'],
                'contentOptions'=>['style'=>'white-space: nowrap;']

            ],
            [
                'attribute' => 'calc_error_code',
                'format' => 'html',
                'filter' => false,
                'contentOptions' => function($dataProvider){
                    if ($dataProvider->status == 2) {
                        return ['class' => 'bg-secondary'];
                    }

                    // Проверка если смета с кодом ошибки то окрашиваем ее
                    if ($dataProvider->calc->error_code == Calc::ERROR_CODE_RESPONSE_WITH_ERROR) {
                        return ['class' => 'warning'];
                    }
                    if ($dataProvider->calc->error_code == Calc::ERROR_CODE_SUCCESS_RESPONSE) {
                        return ['class' => 'success'];
                    }
                    if ($dataProvider->calc->error_code == Calc::ERROR_CODE_NO_RESPONSE) {
                        return ['class' => 'danger'];
                    }
                },
                'value' => function ($model) {
                    if($model->date_repair !== '0000-00-00' && $model->date_repair !== null )
                    {
                        $show_btn =  Html::a('<span class="zoom-btn menu-icon"><a href=""></span>', ['contracts/info','id' => $model->id ]
                        );
                        return $show_btn;
                    }
                    return '';
                }
            ],
            //'buking',
            [
                'label' => 'Букинг',
                'attribute' => 'buking',
                'visible' => Yii::$app->user->can('viewColumn'),
                'filterInputOptions' => ['class' => 'form-control form-control-sm filter-custom'],
            ],
            [
                'label' => 'Релиз',
                'attribute' => 'reliz',
                'visible' => Yii::$app->user->can('viewColumn'),
                'filterInputOptions' => ['class' => 'form-control form-control-sm filter-custom'],
            ],
            //'note:ntext',
            [
                'label' => 'Примечание',
                'attribute' => 'note',
                'visible' => Yii::$app->user->can('viewColumn'),
                'filterInputOptions' => ['class' => 'form-control form-control-sm filter-custom'],
            ],
            [
                'attribute' => 'block',
                'value' => 'blockText',
                'filter' => Containers::getBlockArr(),
                'filterInputOptions' => ['class' => 'form-control form-control-sm filter-custom'],
            ],
            $buttons,
        ],
    ]); ?>
    </div>

    <?php $this->endCache();  endif; ?>

</div>

<script>
    var submit_form = false;

    $('body').on('click', '.search-submit-circle',  function(){
        //enable submit for applyFilter event
        if(submit_form === false) {
            submit_form = true;
            $("#w0").yiiGridView("applyFilter");
        }
    });

    //disable default submit

    $("body").on('beforeFilter', "#w0" , function(event) {
        return submit_form;
    });

    $("body").on('afterFilter', "#w0" , function(event) {
        submit_form = false;
    });

    //$("#w0").removeClass();
    //$(".container").removeClass("container").addClass("container-fluid");
    $(".btn-success").removeClass("btn-success").addClass("btn-primary");
    $(".desc").removeClass();
</script>
