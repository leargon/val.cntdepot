<?php

use yii\helpers\Html;
use app\models\Containers;

$this->title = 'Список загруженных контейнеров';
$this->params['breadcrumbs'][] = $this->title;

?>

<h4><?= $this->title ?></h4>

<div class="containers-info">
        
    <p>
    	<?= Html::a('Загрузить файл', ['#'], ['class' => 'btn btn-success']) ?>
    </p>
    
    <table class="table table-striped table-bordered">
        <tr>
            <th>Сток</th>
            <th>Количество контейнеров</th>
          
        </tr>
        <?php foreach($list as $l): ?>
	        <tr>
	            <td><?= $l->stock_r !== null ? $l->stock_r->name : "Сток удален" ?></td>
	            <td><?= $l->cnt ?></td>
	        </tr>
	    <?php endforeach; ?>
    </table>

</div>