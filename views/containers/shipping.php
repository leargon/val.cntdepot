<?php

use yii\helpers\Html;

$this->title = 'Форма отгрузки контейнера';
$this->params['breadcrumbs'][] = ['label' => 'Контейнеры', 'url' => ['index']];

?>

<div class="containers-create">

    <?= $this->render('_form_shipping', [
        'model' => $model,
    ]) ?>

</div>
