<?php

use yii\helpers\Html;

$this->title = 'Изменить контейнер: ' . $model->number;
$this->params['breadcrumbs'][] = ['label' => 'Контейнеры', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->number, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Редактировать';
?>
<div class="containers-update">

    <h3><?= Html::encode($this->title) ?></h3>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
