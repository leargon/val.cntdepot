<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

$this->title = $model->number;
$this->params['breadcrumbs'][] = ['label' => 'Контейнеры', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="containers-view">

    <h3>Просмотр добавленного контейнера</h3>

    <p>
        <?= Html::a('Изменить', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Действительно удалить запись?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'stock_r.name',
            'number',
            'type_r.name',
            'date_in',
            'time_in',
            'condText',
            'statusText',
            'blockText',
            'note:ntext',
        ],
    ]) ?>

</div>
