<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Orgs;
use app\models\Hoursprice;
?>

<div class="contracts-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'org')->dropDownList(Orgs::getOrgNames()) ?>

    <?= $form->field($model, 'container_size')->dropDownList(Hoursprice::getContainerSize()) ?>

    <?= $form->field($model, 'container_type')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'location')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'element')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'repair_type')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'repair_code')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'repair_desc')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'price')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'hours')->textInput() ?>

    <?= $form->field($model, 'max_count')->textInput() ?>

    <?= $form->field($model, 'loc_code')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
