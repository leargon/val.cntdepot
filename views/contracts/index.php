<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use yii\grid\GridView;
use app\models\Orgs;
use app\models\Contracts;

$this->title = 'Контракты';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="contracts-index">

    <h3><?= Html::encode($this->title) ?></h3>

    <p>
        <?= Html::a('Фильтр', ['#'], ['class' => 'btn btn-success','onclick' => '$( ".filters" ).toggle();return false;']) ?>
        <?= Html::a('Добавить вид работ', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <p>
        <?php
            $model = new Contracts;
            $model->org = $org;

            $form = ActiveForm::begin(['action'=> '', 'method' => 'get']); 

            echo $form->field($model, 'org')->dropDownList(Orgs::getOrgs());
        ?>

            <div class="form-group">
                <?= Html::submitButton('Выбрать', ['class' => 'btn btn-success']) ?>
            </div>

            <?php ActiveForm::end(); ?>
    </p>

    <?php 
    
    if ($dataProvider !== false) :

        echo GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'summary' => false,
            'columns' => [
                'id',
                [
                    'attribute'=>'org_r.name',
                    'filter'=>ArrayHelper::map(Orgs::find()->asArray()->all(), 'name', 'name'),
                ],
                'container_type',
                'container_size',
                'location',
                'element',
                'repair_type',
                'repair_code',
                'repair_desc',
                'price',
                'hours',
                'max_count',
                'loc_code',
                [
                    'class' => 'yii\grid\ActionColumn',
                ],
            ],
        ]); 
    ?>
    <?php else: ?>
        <p>Нет данных</p>
    <?php endif;?>
</div>
<script>
    $('#contracts-org').attr('name','org');
</script>
