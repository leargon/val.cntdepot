<?php

use yii\helpers\Html;

?>
<div id="container-info">
	<table class="table table-striped table-bordered">
		<tr>
			<td>Номер</td>
			<td><?= $container->number ?></td>
		</tr>
		<tr>
			<td>Тип</td>
			<td><?= $container->type_r->name ?></td>
		</tr>
		<tr>
			<td>Размер</td>
			<td><?= $container->type_r->type ?></td>
		</tr>
		<tr>
			<td>Сток</td>
			<td><?= $container->stock_r->name ?></td>
		</tr>
		<tr>
			<td>Дата приема</td>
			<td><?= $container->date_in ?></td>
		</tr>
		<tr>
			<td colspan="2"><?= $container->note ?></td>
		</tr>
	</table>
</div>

<div>
	<p class="text-danger">
		<?php
			if ($contract->error_code == 0) {
				echo "Нет ответа клиента</p>";
				echo Html::a('Редактировать смету', ['containers/contracts','id' => $container->id ], ['class' => 'btn btn-success']);
			}
			if ($contract->error_code == 1) {
				echo "Смета обработана</p>";
				if (Yii::$app->user->can('repairButton')) {
					echo Html::a('Редактировать смету', ['containers/contracts','id' => $container->id ], ['class' => 'btn btn-success']);
				}
			}
			if ($contract->error_code == 2) {
				echo $contract->error_text.'</p>';?>
				<!-- <a href="" class="btn btn-success">Редактировать смету</a> -->
				<?= Html::a('Редактировать смету', ['containers/contracts','id' => $container->id ], ['class' => 'btn btn-success']) ?>
				<?php 
			}
		?>
	

</div>

<div id="contract-info">
	<h5>Результат от: <?= $contract->date_add ?> (
	<?php if($container->status != 2): 
		echo Html::a(
            'Удалить',
            ['delete', 'id' => $contract->id],
            [
            	'data' => [
            	    'confirm' => 'Удалить запись?',
	            	'method' => 'post',
        		],
        	]
        ); 
	endif;
        ?> )</h5>
	<?= stripslashes(htmlspecialchars_decode($contract->html)); ?>
</div>
