<?php

use yii\helpers\Html;

$this->title = 'Редактировать контракт';
$this->params['breadcrumbs'][] = ['label' => 'Контракты', 'url' => ['index']];

?>
<div class="contracts-update">

    <h3><?= Html::encode($this->title) ?></h3>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
