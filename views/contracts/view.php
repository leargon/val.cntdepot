<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

$this->title =  'Контракты';
$this->params['breadcrumbs'][] = ['label' => 'Контракты', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="contracts-view">

    <h3><?= Html::encode($this->title) ?></h3>

    <p>
        <?= Html::a('Обновить', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Действительно удалить этот элемент?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'org',
            'container_type',
            'container_size',
            'location',
            'element',
            'repair_type',
            'repair_code',
            'repair_desc',
            'price',
            'hours',
            'max_count',
            'loc_code',
        ],
    ]) ?>

</div>
