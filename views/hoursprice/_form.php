<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Orgs;
use app\models\Types;
?>

<div class="hoursprice-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'org')->dropDownList(Orgs::getOrgNames()) ?>

    <?= $form->field($model, 'container_size')->dropDownList(Types::getTypeNames()) ?>

    <?= $form->field($model, 'price')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
