<?php

use yii\helpers\Html;

$this->title = 'Добавить в справочник';
$this->params['breadcrumbs'][] = ['label' => 'Нормочасы', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="hoursprice-create">

    <h3><?= Html::encode($this->title) ?></h3>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
