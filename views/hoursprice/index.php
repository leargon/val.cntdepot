<?php

use yii\helpers\Html;
use yii\grid\GridView;


$this->title = 'Нормочасы';
$this->params['breadcrumbs'][] = $this->title;
$buttons =   [
        'class' => 'yii\grid\ActionColumn',
        'template' => '{update} {delete}',
    ];
?>
<div class="hoursprice-index">

    <h3><?= Html::encode($this->title) ?></h3>

    <p>
        <?= Html::a('Нормочасы(добавить в справочник)', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'summary' => false,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'org',
            'container_size',
            'price',

            $buttons
        ],
    ]); ?>
</div>
