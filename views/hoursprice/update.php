<?php

use yii\helpers\Html;

$this->title = 'Изменить значение нормачаса: ' . $model->container_size;
$this->params['breadcrumbs'][] = ['label' => 'Нормочасы', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Изменить';

?>
<div class="hoursprice-update">

    <h3><?= Html::encode($this->title) ?></h3>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
