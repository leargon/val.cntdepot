<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

$this->title = 'Нормочасы';
$this->params['breadcrumbs'][] = ['label' => 'Нормочасы', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="hoursprice-view">


    <p>
        <?= Html::a('Изменить', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Действительно удалить этот элемент?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'org',
            'container_size',
            'price',
        ],
    ]) ?>

</div>
