<?php

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use app\models\Users;
use mdm\admin\components\MenuHelper;
use mdm\admin\components\Helper;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <?php
    NavBar::begin([
        //'brandLabel' => '',
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
            'style' => 'z-index:1;'
        ],
        
    ]);
    $roles = array_keys(Yii::$app->authManager->getRolesByUser(Yii::$app->user->identity->id))[0];
    $menu = [
        ['label' => 'Проверка', 'url' => ['/check/index']],
        ['label' => 'Контейнеры', 'url' => ['/containers/index']],
        ['label' => 'Принять', 'url' => ['/containers/add']],
        ['label' => 'Релизы', 'url' => ['/reliz/index']],
        ['label' => 'Настройки', 'url' => ['/config/index']],
        ['label' => 'История действий', 'url' => ['/logger/index']],
        [
	        'label' => 'Выход (' . \Yii::$app->user->identity->login . ' - ' . $roles. ')',
	        'url' => ['/site/logout'],
	        'linkOptions' => ['data-method' => 'post']
	    ],
    ];

  //   if (!Yii::$app->user->isGuest) :
  //       $roles = Yii::$app->user->identity->roleText;
  //       $menu = [];
  //       if ( Users::ROLE_ADMIN === $roles )
  //       {
  //           $menu = [
  //               ['label' => 'Проверка', 'url' => ['/check/index']],
  //               ['label' => 'Контейнеры', 'url' => ['/containers']],
  //               ['label' => 'Принять', 'url' => ['/containers/add']],
  //               ['label' => 'Релизы', 'url' => ['/reliz']],
  //               ['label' => 'Настройки', 'url' => ['/config/index']],
  //               ['label' => 'История действий', 'url' => ['/logger/index']],   
               
  //               '<li>'
  //                   . Html::beginForm(['/site/logout'], 'post')
  //                   . Html::submitButton(
  //                   'Выйти (' . Yii::$app->user->identity->login . ' - ' . $roles. ')',
  //                   ['class' => 'btn btn-link logout']
  //                   )
  //               . Html::endForm()
  //               . '</li>'
  //           ];
  //       }
  //       elseif ( Users::ROLE_DISPATCHER === $roles  ) 
  //       {
  //           $menu = [
  //               ['label' => 'Проверка', 'url' => ['/check/index']],
  //               ['label' => 'Контейнеры', 'url' => ['/containers']],
  //               ['label' => 'Принять', 'url' => ['/containers/add']],
  //               ['label' => 'Релизы', 'url' => ['/reliz']],
  //               '<li>'
  //                   . Html::beginForm(['/site/logout'], 'post')
  //                   . Html::submitButton(
  //                   'Выйти (' . Yii::$app->user->identity->login . ' - ' . $roles. ')',
  //                   ['class' => 'btn btn-link logout']
  //                   )
  //               . Html::endForm()
  //               . '</li>'
  //           ];
  //       }
  //       elseif ( Users::ROLE_CLIENT === $roles ) {
  //           $menu = [
  //               ['label' => 'Контейнеры', 'url' => ['/containers']],
  //               ['label' => 'Релизы', 'url' => ['/reliz']],
  //               '<li>'
  //                   . Html::beginForm(['/site/logout'], 'post')
  //                   . Html::submitButton(
  //                   'Выйти (' . Yii::$app->user->identity->login . ' - ' . $roles. ')',
  //                   ['class' => 'btn btn-link logout']
  //                   )
  //               . Html::endForm()
  //               . '</li>'
  //           ];
  //       }
  //       else 
  //       {
  //           $menu = [
  //               ['label' => 'Проверка', 'url' => ['/check/index']],
  //               ['label' => 'Контейнеры', 'url' => ['/containers']],
  //               ['label' => 'Принять', 'url' => ['/containers/add']],
  //               ['label' => 'Релизы', 'url' => ['/reliz']],
               
  //               '<li>'
  //                   . Html::beginForm(['/site/logout'], 'post')
  //                   . Html::submitButton(
  //                   'Выйти (' . Yii::$app->user->identity->login . ' - ' . $roles. ')',
  //                   ['class' => 'btn btn-link logout']
  //                   )
  //               . Html::endForm()
  //               . '</li>'
  //           ];
  //           // Users::ROLE_REMZONA
  //       }

  //       // echo Nav::widget([
  //       //     'options' => ['class' => 'navbar-nav navbar-right'],
  //       //     'items' => $menu,
  //       // ]);

  //       echo Nav::widget([
		//     'items' => MenuHelper::getAssignedMenu(Yii::$app->user->id),
		//     'options' => ['class' => 'navbar-nav navbar-right'],
		// ]);

  //   endif;
    $menuItems = Helper::filter($menu);
    echo Nav::widget([
	    'items' => $menuItems,
	    'options' => ['class' => 'navbar-nav navbar-right'],
	]);
    NavBar::end();

    ?>

    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>

        <?php if ((Yii::$app->controller->id == 'config')): ?>
            <?= $this->context->renderPartial('/layouts/secondMenu'); ?>
        <?php endif; ?>

        <?= $content ?>
    </div>
</div>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
