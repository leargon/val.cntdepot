<?php

use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;

?>

 <div>
    <?php echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right second-bar'],
            'items' => [
                ['label' => 'Контракты по ремонту', 'url' => ['/contracts']],
                ['label' => 'Пользователи', 'url' => ['/user/index']],
                ['label' => 'Стоки', 'url' => ['/stock/index']],
                ['label' => 'Типы контейнеров', 'url' => ['/type/index']],
                ['label' => 'Импорт', 'url' => ['/config/import']],
                ['label' => 'Организации', 'url' => ['/orgs/index']],
                ['label' => 'Сообщения', 'url' => ['/messages/index']],
                ['label' => 'Справочник стоимости нормочасов', 'url' => ['/hoursprice/index']],
                ['label' => 'Загрузка списка контейнеров(XLS)', 'url' => ['/config/import_xls']],
                ['label' => 'Привязка ролей', 'url' => ['/admin']],
                ['label' => 'Фильтр доступа', 'url' => ['/admin/route']],
                ['label' => 'Разрешения', 'url' => ['/admin/permission']],
                ['label' => 'Роли', 'url' => ['/admin/role']],
                ['label' => 'Меню', 'url' => ['/admin/menu']],
        ],
    ]); ?>
</div>