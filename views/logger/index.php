<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\models\Logger;
use kartik\date\DatePicker;

$this->title = 'История действий';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="logger-index">

    <h3><?= Html::encode($this->title) ?></h3>

    <p>
        <?= Html::a('Фильтр', ['#'], ['class' => 'btn btn-success','onclick' => '$( ".filters" ).toggle();return false;']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'summary' => false,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'user.login',
            'note',
            [
                'attribute' => 'type',
                'value' => 'typeText',
                'filter' => Logger::getTypeArr(),
                'filterInputOptions' => ['class' => 'form-control form-control-sm', 'style' => 'width: 114px;'],
            ],
            [
                'attribute' => 'action',
                'value' => 'actionText',
                'filter' => Logger::getActionArr(),
                'filterInputOptions' => ['class' => 'form-control form-control-sm', 'style' => 'width: 142px;'],
            ],
            'ip',
            [
                'attribute' => 'date_add',
                'format' => 'datetime',
                'value' => 'date_add',
                'headerOptions' => [
                    'class' => 'col-md-2'
                ],
                'filter' => DatePicker::widget([
                    'model' => $searchModel,
                    'attribute' => 'date_add_s',
                    'attribute2' => 'date_add_e',
                    'options' => ['placeholder' => 'От'],
                    'options2' => ['placeholder' => 'До'],
                    'type' => DatePicker::TYPE_RANGE,
                    'separator' => '-',
                    'pluginOptions' => ['format' => 'yyyy-mm-dd'],
                ]),
                'format' => ['date', 'Y-MM-dd HH:mm'],
                'filterInputOptions' => ['class' => 'form-control form-control-sm date-filter'],

            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view}',
            ],
        ],
    ]); ?>
</div>
