<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

$this->title = 'История';
$this->params['breadcrumbs'][] = ['label' => 'История', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Просмотр записи';
?>
<div class="logger-view">

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            [
                'label' => 'Пользователь',
                'attribute' => 'user.login',
            ],
            'type',
            'actionText',
            'ip',
            'date_add',
            'note',
            'text:ntext',
        ],
    ]) ?>

</div>
