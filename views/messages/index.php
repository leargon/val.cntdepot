<?php

use yii\helpers\Html;
use yii\grid\GridView;

$this->title = 'Сообщения';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="messages-index">

    <h5><?= Html::encode($this->title) ?></h5>

    <p>
        <?= Html::a('Фильтр', ['#'], ['class' => 'btn btn-success','onclick' => '$( ".filters" ).toggle();return false;']) ?></p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'summary' => false,
        'columns' => [

            'to',
            'from',
            'subject',
            'create',
            'send',
            [
                'class' => 'yii\grid\ActionColumn',
            ],
        ],
    ]); ?>
</div>
