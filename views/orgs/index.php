<?php

use yii\helpers\Html;
use yii\grid\GridView;

$this->title = 'Организации';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="orgs-index">

    <h3><?= Html::encode($this->title) ?></h3>

    <p>
        <?= Html::a('Добавить организацию', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'summary' => false,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'name',
            'email',
            'identifier',
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
