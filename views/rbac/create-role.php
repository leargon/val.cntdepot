<?php
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use app\models\RoleForm;

$this->title = 'Добавление роли';
$this->params['breadcrumbs'][] = ['label' => 'Роли', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="users-create">

    <h3><?= Html::encode($this->title) ?></h3>

    <?php $form = ActiveForm::begin() ?>
    
    <?= $form->field($role, 'name') ?>
    <?= $form->field($role, 'description') ?>
    
    <?= $form->field($role, 'permission')->checkboxList(
            RoleForm::getPermissions(),
            ['separator' => '<br>']
    )->label('Какие разрешения добавить к роли?')
    ?>
    <?= Html::submitButton('Создать', ['class' => 'btn btn-success']) ?>

	<?php ActiveForm::end() ?>

</div>
