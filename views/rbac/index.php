<?php

use yii\helpers\Html;
use yii\grid\GridView;
$this->title = 'Роли';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="users-index">

    <h3><?= Html::encode($this->title) ?></h3>

    <p>
        <?= Html::a('Добавить', ['create-role'], ['class' => 'btn btn-success']) ?>
    </p>

	<?= GridView::widget([
        'dataProvider' => $provider,
        'columns' => [
            'name',
            'description',
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
