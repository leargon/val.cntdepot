<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Stock;
use app\models\Types;
use app\models\Reliz;

use kartik\date\DatePicker;

?>

<div class="reliz-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'stock')->dropDownList(Stock::getStockNames()) ?>

    <?= $form->field($model, 'number')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'buking')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'type_cnt')->dropDownList(Types::getTypeNames()) ?>

    <?= $form->field($model, 'amount')->textInput() ?>


    <?= $form->field($model, 'date')->widget(DatePicker::classname(), [
        'options' => ['placeholder' => ''],
        'pluginOptions' => [
            'autoclose'=>true,
            'format' => 'yyyy-mm-dd'
        ]
    ]) ?>

    <?= $form->field($model, 'date_to')->widget(DatePicker::classname(), [
        'options' => ['placeholder' => ''],
        'pluginOptions' => [
            'autoclose'=>true,
            'format' => 'yyyy-mm-dd',
        ]
    ]) ?>

    <?= $form->field($model, 'method')->dropDownList(Reliz::getMethodArr()) ?>

    <div id="am_edit">
        <?php if(isset($autos)) : ?>
            <p>
                <a href="#reliz-method" onclick="addAm();return false;">Добавить</a>
            </p>
            <?php foreach($autos as $auto): ?>
                <div>
                    Номер: <input class="form-control driver-input" type="text" name="number[]" value="<?=$auto->number?>"/>
                    Водитель: <input class="form-control driver-input" type="text" name="driver[]" value="<?=$auto->driver?>"/>
                    <a href="#" onclick="$(this).parent().remove(); return false;">Удалить</a>
                </div>
            <?php endforeach; ?>
        <?php endif; ?>
    </div>

    <?= $form->field($model, 'comment')->textarea(['rows' => 6]) ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<script type="text/javascript">
    function addAm()
    {
        $( "#am_edit" ).append( 'Номер: <input class="form-control driver-input" type="text" name="number[]" value=""/> ' );
        $( "#am_edit" ).append( 'Водитель: <input class="form-control driver-input" type="text" name="driver[]" value=""/><br/>' );
    }
    $('#reliz-method').change(function() {
        if($('#reliz-method').val() == 1) {
            if($('#am_edit').val().trim() == '') {
                $( "#am_edit" ).append('<p><a href="#reliz-method" onclick="addAm();return false;">Добавить</a></p>');
                var cnt = $('#reliz-amount').val() == '' ? 1 : $('#reliz-amount').val();
                var i = 0;
                while (i < cnt) {
                  addAm();
                  i++;
                }
            }
        }
    })
</script>