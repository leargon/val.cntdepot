<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Stock;
use app\models\Types;
use app\models\Reliz;

use kartik\date\DatePicker;

?>

<div class="reliz-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'method')->dropDownList(Reliz::getMethodArr()) ?>

    <?= $form->field($model, 'stock')->dropDownList(Stock::getStocks()) ?>

    <?= $form->field($model, 'number')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'buking')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'type_cnt')->dropDownList(Types::getTypes()) ?>

    <?= $form->field($model, 'amount')->textInput() ?>


    <?= $form->field($model, 'date')->widget(DatePicker::classname(), [
        'options' => ['placeholder' => ''],
        'pluginOptions' => [
            'autoclose'=>true
        ]
    ]) ?>

    <?= $form->field($model, 'date_to')->widget(DatePicker::classname(), [
        'options' => ['placeholder' => ''],
        'pluginOptions' => [
            'autoclose'=>true
        ]
    ]) ?>

    <?= $form->field($model, 'status')->textInput() ?>

    <?= $form->field($model, 'comment')->textarea(['rows' => 6]) ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
