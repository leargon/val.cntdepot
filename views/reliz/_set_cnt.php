<?php

use yii\helpers\Html;
use yii\helpers\Url;

?>

<div>

	<table class="table table-striped table-bordered">
		<tr>
			<td>Релиз</td>
			<td>Букинг</td>
			<td>Тип <br>контейнера</td>
			<td>Комментарий</td>
			<td>№ АМ</td>
			<td>Водитель</td>
		</tr>
		<tr>
			<td><?= $reliz->number ?></td>
			<td><?= $reliz->buking ?></td>
			<td><?= $reliz->type_cnt ?></td>
			<td><?= $reliz->comment ?></td>
			<td><?= $auto->number ?></td>
			<td><?= $auto->driver ?></td>
		</tr>
	</table>

	<p>
		Найти контейнер: <input type="text" name="cnt-val" class="cnt-val" value=""/> 
		<button id="search-btn" class="btn btn-primary reliz-search-btn">Поиск</button>
	</p>

	<div class="container-search">
		<?= $this->context->renderPartial('/check/custom', 
			[ 
				'containers' 	=> $containers, 
				'reliz' 		=> $reliz, 
				'auto' 			=> $auto
			]); ?>
	</div>

</div>

<script type="text/javascript">

	$('#search-btn').click(function(){
		var search_number = $('.cnt-val').val();

		var params = "";
		params += "&number=" 	+ search_number;
		params += "&reliz_id=" 	+ <?= $reliz->id ?>;
		params += "&auto_id=" 	+ <?= $auto->id ?>;

        $.ajax({
            url: "<?= Url::toRoute(['check/ajax-custom'])?>" + params,
        }).done(function(data) {
            $('.container-search').html(data);
        });
      })

</script>