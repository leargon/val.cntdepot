<?php

use yii\helpers\Html;

$this->title = 'Добавить';
$this->params['breadcrumbs'][] = ['label' => 'Релизы', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="reliz-create">

    <h3><?= Html::encode($this->title) ?></h3>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
