<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\models\Users;
use app\models\Stock;
use app\models\Types;
use app\models\Containers;
use app\models\Reliz;
use yii\helpers\ArrayHelper;
use kartik\date\DatePicker;
use mdm\admin\components\Helper;

$this->title = 'Релизы';
$this->params['breadcrumbs'][] = $this->title;

$role = Yii::$app->user->identity->roleText;

$buttons['template'] =  Helper::filterActionColumn('{reliz}{update}{delete}{view}');
// $buttons['template'] = '{reliz} {update} {delete} {view}';
$buttons['class']    = 'yii\grid\ActionColumn';
$buttons['buttons']  = [
    'update' => function ($url, $model) {
        return Html::a('<span class="update-btn menu-icon"></span>', ['update','id' => $model->id ]);
    },
    'delete' => function ($url, $model) {
        return Html::a('<span class="trash-btn menu-icon"></span>', 
            ['delete','id' => $model->id ],
            [
                    'data' => [
                        'confirm' => 'Удалить запись?',
                        'method' => 'post',
                    ]
            ]);
    },
    'view' => function ($url, $model) {
        return Html::a('<span class="info-btn menu-icon"></span>', ['view','id' => $model->id ]);
    },
    'reliz' => function ($url, $model) {
        return Html::a('<span class="shipping-btn menu-icon"></span>', ['reliz','id' => $model->id ]);
    }
];
?>

<div class="reliz-index">

    <p>
        <?= Html::a('Фильтр', ['#'], ['class' => 'btn btn-success','onclick' => '$( ".filters" ).toggle();return false;']) ?>
        <?= Html::a('Добавить', ['create'], ['class' => 'btn btn-success']) ?>
        <?= Html::a('Сбросить фильтр', ['index'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php if ($this->beginCache('StockStatus_' . $role)) : ?>

        <h5>Состояние стока</h5>
        <table class="table table-striped table-bordered stock-condition">
            <?php
                $types = Types::getTypes();
                $stocks = Stock::getStocks();
            ?>
            <tr>
                <td>Сток</td>
                <?php foreach($types as $t): ?>
                    <td><?= $t ?></td>
                <?php endforeach;?>
            </tr>
            <?php foreach($stocks as $stock_id => $stock): ?>
            <tr>
                <td><?= $stock ?></td>
                <?php foreach($types as $tid => $type):?>
                    <?php 
                        $status = Stock::getStockStatus(['name'=>$stock, 'id'=>$stock_id], $tid, $type);
                        if ($status === 0)
                        {
                            $class_color = 'c_yellow';
                        } else 
                        $class_color = $status > 0 ? 'c_green' : 'c_red' ;
                            
                    ?>
                        <td><span class="<?= $class_color ?> stat_color"><?= $status ?></span></td>
                <?php endforeach;?>
            </tr>
            <?php endforeach;?>
        </table>

    <?php $this->endCache();  endif; ?>

    <?php 
        $page = isset($_GET['page']) ? $_GET['page'] : 1;
        $sort = isset($_GET['sort']) ? $_GET['sort'] : 1;
        $cache_key = 'RelizIndex_' . $role . '_' . $page . '_' . $sort;
        if ($this->beginCache($cache_key)) : 
    ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'summary' => false,
        'tableOptions' => [
            'class' => 'table table-striped table-bordered data-table'
        ],
        'columns' => [
            'number',
            [
                'attribute' => 'type',
                'value' => 'typeText',
                'filter' => Reliz::getTypeArr(),
                'filterInputOptions' => ['class' => 'form-control form-control-sm', 'style' => 'width: 114px;'],
            ],
            [
                'attribute' => 'stock',
                'filter' => ArrayHelper::map(Stock::find()->all(), 'name', 'name'),
                'filterInputOptions' => ['class' => 'form-control form-control-sm', 'style' => 'width: 135px;'],
            ],
            'buking',
            [
                'attribute' => 'type_cnt',
                'filter' => ArrayHelper::map(Types::find()->all(), 'name', 'name'),
                'filterInputOptions' => ['class' => 'form-control form-control-sm', 'style' => 'width: 80px;'],
            ],
            'amount',
            'restCount',
            [
                'attribute' => 'date_to',
                'value' => function ($model) {
                   return  $model->getReceptionDate();
                },
                'headerOptions' => [
                    'style' => 'display:inline-block;width: 160px;'
                ],
                'filterInputOptions' => ['class' => 'form-control form-control-sm', 'style' => 'width: 150px;'],

            ],
            [
                'attribute' => 'method',
                'value' => 'methodText',
                'filter' => Reliz::getMethodArr(),
                'filterInputOptions' => ['class' => 'form-control form-control-sm', 'style' => 'width: 135px;'],
            ],
            'comment:ntext',
            [
                'attribute' => 'status',
                'value' => 'statusText',
                'filter' => Reliz::getStatusArr(),
                'headerOptions' => [
                    'style' => 'width: 100px;'
                ],
                'filterInputOptions' => ['class' => 'form-control form-control-sm', 'style' => 'width: 114px;'],
                'contentOptions' => function($dataProvider){
                    if ($dataProvider->status == 0) {
                        return ['class' => 'warning'];
                    }
                    if ($dataProvider->status == 1) {
                        return ['class' => 'success'];
                    }
                }
            ],
            $buttons,
        ],
    ]); ?>

    <?php $this->endCache();  endif; ?>

</div>

<script type="text/javascript">
    $(".reliz-index h5").click(function(){
        $(".stock-condition").toggle();
    });
</script>
