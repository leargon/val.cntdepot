<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;
use yii\bootstrap\Modal;

$this->title = $model->number;
$this->params['breadcrumbs'][] = ['label' => 'Релизы', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="reliz-view">

    <table class="table table-striped table-bordered">
        <tr>
            <td>Релиз</td>
            <td>Тип</td>
            <td>Сток</td>
            <td>Букинг</td>
            <td>Тип контейнера</td>
            <td>К-во конт</td>
            <td>Остаток конт</td>
            <td>Дата выдачи</td>
            <td>Способ отгрузки</td>
            <td>Комментарий</td>
            <td>Статус</td>
        </tr>
        <tr>
            <td><?= $model->number ?></td>
            <td><?= $model->typeText ?></td>
            <td><?= $model->stock ?></td>
            <td><?= $model->buking ?></td>
            <td><?= $model->type_cnt ?></td>
            <td><?= $model->amount ?></td>
            <td><?= $model->restCount ?></td>
            <td><?= $model->date_to ?></td>
            <td><?= $model->methodText ?></td>
            <td><?= $model->comment ?></td>
            <td><?= $model->statusText ?></td>
        </tr>
    </table> 

    <div id="count_cont">
        <h5>
            Количество контейнеров доступных к отправке: 
            <?= $model->amount - $model->countByReliz ?>
        </h5>
    </div>

    <h5>АМ/Водитель</h5>

    <table class="table table-striped table-bordered">
        <tr>
            <td>Релиз</td>
            <td>№ АМ</td>
            <td>Водитель</td>
            <td></td>
        </tr>
        <?php foreach($autos as $auto): ?>
            <?php if($auto->checkContainer()) continue; ?>
            <tr>
                <td><?= $model->number ?></td>
                <td><?= $auto->number ?></td>
                <td><?= $auto->driver ?></td>
                <td>
                    <div class="set-cnt" auto-id="<?= $auto->id; ?>">

                            <div data-toggle="modal" data-target="#w0">
                                <span class="shipping-btn menu-icon"></span>
                            </div>

                    </div>
                </td>
            </tr>
        <?php endforeach; ?>
    </table>
    <script type="text/javascript">

        $('.set-cnt').click(function() {

            var params = "&auto_id=" + $(this).attr('auto-id');

            $.ajax({
                url: "<?= Url::toRoute(['set-cnt', 'reliz_id'=>$model->id])?>" + params,
            }).done(function(data) {
                $('.modal-data').html(data);
            });
        })
        
    </script>

    <?php Modal::begin([
        'header' => '<h5>Назначить контейнер</h5>',
        'size' => 'modal-lg',
    ]);?>
     
    <div class="modal-data"></div>
     
    <?php Modal::end();?>

</div>
