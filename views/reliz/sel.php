<?php

use yii\helpers\Html;

$this->params['breadcrumbs'][] = ['label' => 'Релизы', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->number, 'url' => ['view', 'id' => $model->id]];
?>
<div class="reliz-update">

    <?= $this->render('_form_sel', [
        'model' => $model,
    ]) ?>

</div>
