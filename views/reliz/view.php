<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

$this->title = $model->number;
$this->params['breadcrumbs'][] = ['label' => 'Релизы', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="reliz-view">

    <h5><?= Html::encode($this->title) ?></h5>

    <p>
        <?= Html::a('Изменить', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Действительно удалить эту запись?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'stock',
            'type_cnt',
            'amount',
            'rest',
            'date',
            'date_to',
            'number',
            'buking',
            'method',
            'type',
            'status',
            'comment:ntext',
        ],
    ]) ?>

</div>
