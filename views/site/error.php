<?php

use yii\helpers\Html;

$this->title = $name;
?>
<div class="site-error">

    <h3><?= Html::encode($this->title) ?></h3>

    <div class="alert alert-danger">
        <?= nl2br(Html::encode($message)) ?>
    </div>


</div>
