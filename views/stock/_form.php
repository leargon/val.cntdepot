<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Users;

?>

<div class="stock-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'segment')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'uid')->dropDownList(Users::getUsers()) ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
