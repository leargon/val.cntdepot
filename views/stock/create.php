<?php

use yii\helpers\Html;

$this->title = 'Добавить сток';
$this->params['breadcrumbs'][] = ['label' => 'Стоки', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="stock-create">

    <h3><?= Html::encode($this->title) ?></h3>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
