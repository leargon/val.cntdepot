<?php

use yii\helpers\Html;
use yii\grid\GridView;

$this->title = 'Стоки';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="stock-index">

    <h3><?= Html::encode($this->title) ?></h3>

    <p>
        <?= Html::a('Добавить', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'summary' => false,
        'columns' => [
            'id',
            'name',
            'segment',
            [
                'attribute'=>'user.org_r.name',
                'label'=>'Организация',
            ],
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
