<?php

use yii\helpers\Html;

$this->title = 'Добавление типа контейнера';
$this->params['breadcrumbs'][] = ['label' => 'Типы контейнеров', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="types-create">

    <h3><?= Html::encode($this->title) ?></h3>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
