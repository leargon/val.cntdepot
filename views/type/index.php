<?php

use yii\helpers\Html;
use yii\grid\GridView;

$this->title = 'Типы контейнеров';
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="types-index">

    <h3><?= Html::encode($this->title) ?></h3>

    <p>
        <?= Html::a('Добавить', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'summary' => false,
        'columns' => [
            'name',
            'iso',
            'type',
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
