<?php

use yii\helpers\Html;

$this->title = 'Редактирование типа контейнера: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Типы контейнеров', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Редактировать';
?>
<div class="types-update">

    <h3><?= Html::encode($this->title) ?></h3>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
