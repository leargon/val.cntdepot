<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Users;
use app\models\Orgs;

?>

<div class="users-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'login')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'new_password')->passwordInput(['maxlength' => true]) ?>
    
    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'organization')->dropDownList(Orgs::getOrgs(), ['prompt'=>'']) ?>

    <?= $form->field($model, 'edi')->dropDownList(Users::getEdiArr(), ['prompt'=>'']) ?>

    <?= $form->field($model, 'role')->dropDownList(Users::getRoleArr(), ['prompt'=>'Выберите группу пользователя']) ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
