<?php

use yii\helpers\Html;
use yii\grid\GridView;
$this->title = 'Пользователи';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="users-index">

    <h3><?= Html::encode($this->title) ?></h3>

    <p>
        <?= Html::a('Добавить', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'summary' => false,
        'columns' => [
            'login',
            [
                'attribute' => 'org',
                'value' => 'org_r.name',
            ],
            'roleText',
            'ediText',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
