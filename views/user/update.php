<?php

use yii\helpers\Html;

$this->title = 'Редактирование профиля пользователя: ' . $model->login;
$this->params['breadcrumbs'][] = ['label' => 'пользователи', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->login, 'url' => ['view', 'id' => $model->id]];
?>
<div class="users-update">

    <h3><?= Html::encode($this->title) ?></h3>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
