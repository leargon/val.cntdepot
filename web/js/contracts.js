var repair = true;
function updatePrice(val) {
	var maxPrice = $('.contract-selected').attr("data-currentprice");
	if ( val > maxPrice ) {
		val = maxPrice;
	}
	$('.contract-selected').attr("data-currentprice", val);
}

function updateCount(val) {
	var maxCount = $('.contract-selected').attr("data-maxcount");
	if ( val > maxCount ) {
		val = maxCount;
	}
	$('.contract-selected').attr("data-currentcount", val);
}

function payment() {
    $(".repair-class").remove();
    var id = $(".repair-selected").attr("data-id");
    var all_hours = 0;
    $(".contract-list").each(function( i ) { 
	    all_hours += $( this ).attr("data-hours") * $( this ).attr("data-currentcount");
	});
	$.ajax({
		url: url_get_contracts_repair + all_hours,
	}).done(function(data) {
		$("li").removeClass("contract-selected");
		$("#types-selected").prepend(data);
		editContract($(".contract-selected"));
		payment_add();
	});
}

function payment_add() {
    
	$(".contract-table").show();
	$(".cnt-print").remove();

	var all_price = job_price =  all_hours = 0;
	var full_price = full_job_price = 0;

	$(".contract-list").each(function( i ) {

		all_price = $( this ).attr("data-currentprice") * $( this ).attr("data-currentcount");
		job_price = $( this ).attr("data-hours") * hours_price * $( this ).attr("data-currentcount");
		all_price += job_price;

		full_price 		+= all_price;
		full_job_price 	+= job_price;
		all_hours 		+= parseFloat($( this ).attr("data-hours")) * $( this ).attr("data-currentcount");

		var row = "";
		row += "<tr class=\"cnt-print\">";
		row += "<td>" + ++i + "</td>"
		row += "<td>" + $( this ).attr("data-currentcount") + "</td>"
		row += "<td>" + $( this ).text() + "</td>"
		row += "<td>" + $( this ).attr("data-repaircode") + "</td>"
		row += "<td>" + $( this ).attr("data-loccode") + "</td>"
		row += "<td>" + $( this ).attr("data-currentprice") + "</td>"
		row += "<td>" + $( this ).attr("data-hours") + "</td>"
		row += "<td>" + all_price.toFixed(2) + "</td>"
		row += "</tr>"

		$(".contract-table tr:last").after(row);

	});

	var row = "";
	row += "<tr class=\"cnt-print thead last-row\">";
	row += "<td colspan=\"5\"></td>";
	row += "<td>Итого</td>";
	row += "<td id=\"full_job\">" + all_hours.toFixed(2) + "</td>";
	row += "<td id=\"full_price\">"+ full_price.toFixed(2) + "</td></tr>";

	$(".contract-table tr:last").after(row);
	$(".repair_btn").show();
}

function saveContracts() {
	var row = "<table>";
	row += "<tr class=\"thead\">";
	row += "<td>Номер, пп</td>";
	row += "<td>Кол-во, шт</td>";
	row += "<td>Описание</td>";
	row += "<td>Код ремонта</td>";
	row += "<td>Loc Code</td>";
	row += "<td>Материал за шт.</td>";
	row += "<td>Нормочасов за шт</td>";
	row += "<td>Итого по виду работ</td>";
	row += "</tr>";

	var all_price = job_price = 0; 

	$(".contract-list").each(function( i ) {

		all_price = $( this ).attr("data-currentprice") * $( this ).attr("data-currentcount");
		job_price = parseFloat($( this ).attr("data-hours")) * hours_price* $( this ).attr("data-currentcount");
		all_price += job_price;

		row += "<tr>";
		row += "<td>" + ++i + "</td>"
		row += "<td>" + $( this ).attr("data-currentcount") + "</td>"
		row += "<td>" + $( this ).text() + "</td>"
		row += "<td>" + $( this ).attr("data-repaircode") + "</td>"
		row += "<td>" + $( this ).attr("data-loccode") + "</td>"
		row += "<td>" + $( this ).attr("data-currentprice") + "</td>"
		row += "<td>" + $( this ).attr("data-hours") + "</td>"
		row += "<td>" + Math.round(all_price * 100) / 100 + "</td>"
		row += "</tr>"
	});

	row += $(".last-row").html();
	row += "</table>";

	var current_date = $("#current").prop("checked") ? "&current-date=1" : "";
			
	$.ajax({
		url: url_save,
		type: "POST",
		data: "table=" + row + "&repair_date=" + $('.date-input').val() + "&container_id=" + container_id  + current_date,
	}).done(function(data) {
		window.location.href = url_index;
	});

	return false;
}

function editContract(item) {
	$(".contract-list").removeClass("contract-selected");
	$(item).addClass("contract-selected");
	var params = "";
	params = "<p>Максимальное количество: " + $(item).attr("data-maxcount") + "</p>";
	params += "<p>Максимальная стоимость материала:" + $(item).attr("data-price") + "</p>";
	params += "<p>Нормочасы за единицу: " + $(item).attr("data-hours") + "</p>";
	params += "<p>Кол-во: <input type='number' onchange='updateCount($(this).val())' value=\"" + $(item).attr("data-currentcount") + "\" max='" + $(item).attr("data-maxcount") + "'/></p>";
	params += "<p>Материалы за шт: <input type='number' onchange='updatePrice($(this).val())' value=\"" + $(item).attr("data-currentprice") + "\" max='" + $(item).attr("data-price") + "'/></p>";
	params += "<p><button onclick='payment();'>Расчитать</button></p>";
	params += "<p class=\"repair_date repair_btn\"><input id=\"current\" type=\"checkbox\" name=\"current-date\" checked/>  Дата ремонта: <input class=\"date-input\" value=\"" + current_date + "\"><p>";
	params += "<button class=\"repair_btn\" onclick=\"saveContracts();\">Сохранить</button></p></p>";
	$("#contract-params").html(params);
}

function contract_add() {
	var id = $(".repair-selected").attr("data-id");
	$.ajax({
		url: url_get_contracts + id,
	}).done(function(data) {
		$("li").removeClass("contract-selected");
		$("#types-selected").prepend(data);
		editContract($(".contract-selected"));
	});
}

$( ".repair-type" ).click(function() {
	$("li").removeClass("repair-selected");
	$(this).addClass("repair-selected");
});

// $( ".repair-type" ).dblclick(function() {
// 	var id = $(this).attr('data-id');
// 	console.log(id);
// 	$("li").removeClass("repair-selected");
// 	$(this).addClass("repair-selected");
// 	if( !$('.contract-list')[0] ) {
// 		contract_add_repair();
// 	}
// 	contract_add();
// });

$( ".contract-add" ).click(function() {
	contract_add();
	//$('.repair-selected').parent().parent().slideToggle();
});

$( ".contract-list" ).click(function() {
	$("li").removeClass("contract-selected");
	$(this).addClass("contract-selected");
});

$( ".location" ).click(function() {
	$(this).next().toggle();
	$('.location').not(this).next().hide();
	$('.element').next().hide();
});

$( ".element" ).click(function() {
	$(this).next().toggle();
});

$( ".contract-del" ).click(function() { 
	$( ".contract-selected" ).remove();
	$( "#contract-params" ).html("");
});
